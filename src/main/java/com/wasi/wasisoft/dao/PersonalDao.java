package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonalDao extends JpaRepository<Personal, Long> {
    @SuppressWarnings("unchecked")
    Personal save(Personal personal);

    @Query ("SELECT p FROM Personal  p WHERE p.idarea = 2")
    List<Personal> findPersonalByAreaRehabilitacion();

    List<Personal> findAllByIdarea(Area idarea);
}


