package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BiometriaDao extends JpaRepository<Biometria, Long>{

    List<Biometria> findByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapifisica);

}
