package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaEducacion;
import com.wasi.wasisoft.model.SaludActual;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HaeverFernandez on 02/05/2018.
 */
public interface SaludActualDao extends JpaRepository<SaludActual, Long> {

    List<SaludActual> findByIdfichaeduc(FichaEducacion idfichaeducacion);
}
