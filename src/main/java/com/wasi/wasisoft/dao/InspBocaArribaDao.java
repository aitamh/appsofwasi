package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.InspBocaArriba;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InspBocaArribaDao extends JpaRepository<InspBocaArriba, Long>{

    List<InspBocaArriba> findByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);
}
