package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Fuerza;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface FuerzaDao extends JpaRepository<Fuerza, Long>{

    List<Fuerza> findByFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica);
}
