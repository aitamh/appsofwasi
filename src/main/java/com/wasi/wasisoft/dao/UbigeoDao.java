package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Ubigeo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UbigeoDao extends JpaRepository<Ubigeo,Long>{

//    @Query("SELECT DISTINCT u.id_depa, u.nombre FROM Ubigeo u WHERE u.id_prov=0 and u.id_dist=0")

//    List<Ubigeo> findDepartamentos();

//    @Query("SELECT u.id_prov,u.nombre FROM ubigeo u WHERE u.id_depa = :id_depa and u.id_dist=0")
//    @Query("SELECT u.id_prov,u.nombre FROM ubigeo u WHERE u.id_dist=0")
//    List<Ubigeo> findById_depa( String id_depa);

}
