package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Medicamento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicamentoDao extends JpaRepository<Medicamento, Long>{
}
