package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.InstrEvaluacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
public interface InstrEvaluacionDao extends JpaRepository<InstrEvaluacion, Long> {

    @Query("SELECT e FROM InstrEvaluacion e ORDER BY e.nombreprueba")
    List<InstrEvaluacion> findAllOrOrderByNombreprueba();
}
