package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.MovilidadArticulaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MovilidadArticulacionesDao extends JpaRepository<MovilidadArticulaciones,Long>{

    List<MovilidadArticulaciones> findByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);

//    @Query("SELECT m.movizquierda FROM MovilidadArticulaciones m where m.idmovarticulaciones =:idmovarticulaciones")
//    List<MovilidadArticulaciones> findByIdmovarticulaciones(@Param("idmovarticulaciones") Long idmovarticulaciones);


}

