package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ResultadoPruebas;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
public interface ResultadoPruebasDao extends JpaRepository<ResultadoPruebas, Long> {

    List<ResultadoPruebas> findAllByIdexpedienteAndAndIdarea(Expediente idexpediente, Area idarea);

}
