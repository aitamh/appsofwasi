package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.DesarrolloInd;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DesarrolloIndDao extends JpaRepository<DesarrolloInd, Long> {

    List<DesarrolloInd> findByIdanamnesis(AnamnesisClinico idanamnesis);
}
