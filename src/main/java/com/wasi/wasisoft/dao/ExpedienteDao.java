package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import org.hibernate.boot.spi.InFlightMetadataCollector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExpedienteDao extends JpaRepository<Expediente, Long>{

    List<Expediente> findByCodigohistorial(String codigoHistoria);

    List<Expediente> findByDni(int dni);

    @Query ("SELECT COUNT(e.sexo) as cantidad FROM Expediente e GROUP BY e.sexo")
    List<Expediente> findExpedienteBySexo();

    //List<Expediente> findByNombreAndPrimer_apellido(String nombre, String primerapellido);
//
//    @Query("SELECT DISTINCT u.nombre FROM Ubigeo u WHERE u.id_prov=0 and u.id_dist=0")
//    public List<Ubigeo> findDepartamentos();

    //@Query("SELECT e.sexo FROM Expediente e")
//    @Query ("select e.sexo, COUNT(e.sexo) FROM Expediente e WHERE e.sexo = 'Femenino'")

}
