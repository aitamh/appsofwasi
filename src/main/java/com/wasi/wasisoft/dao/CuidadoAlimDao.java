package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.CuidadoAlim;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaSalud;
import com.wasi.wasisoft.model.Fichacentrodia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
public interface CuidadoAlimDao extends JpaRepository<CuidadoAlim, Long> {

//    List<CuidadoAlim> findAllByIdfichasalud(FichaSalud idfichasalud);
//
        List<CuidadoAlim> findAllByIdcuidalim(CuidadoAlim cuidadoAlim);

        List<CuidadoAlim> findAllByIdexpediente(Expediente idexpediente);
}
