package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Fuerza;
import com.wasi.wasisoft.model.FuerzaSeguimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface FuerzaSeguimientoDao extends JpaRepository<FuerzaSeguimiento, Long> {

    List<FuerzaSeguimiento> findByFuerza(Fuerza fuerza);

    @Query("SELECT d.idfuerza FROM FuerzaSeguimiento f INNER JOIN f.fuerza d ")
    List<FuerzaSeguimiento> getprueba();


}
