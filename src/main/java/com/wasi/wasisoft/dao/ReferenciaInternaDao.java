package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaInterna;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
public interface ReferenciaInternaDao extends JpaRepository<ReferenciaInterna, Long>{

    List<ReferenciaInterna> findByIdareaAndIdexpediente(Area idarea, Expediente idexpediente);

    List<ReferenciaInterna> findByIdarea(Area idarea);

    List<ReferenciaInterna> findAllByIdexpediente(Expediente idexpediente);
}
