package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.CuidadoAlim;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichacentrodia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 27/04/2018.
 */
public interface FichacentrodiaDao extends JpaRepository<Fichacentrodia, Long> {

    List<Fichacentrodia> findAllByIdcuidalim(CuidadoAlim idcuidadoalim);

}
