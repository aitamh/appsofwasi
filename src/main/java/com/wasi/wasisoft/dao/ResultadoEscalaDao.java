package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.ResultadoEscala;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface ResultadoEscalaDao extends JpaRepository<ResultadoEscala, Long>{

    List<ResultadoEscala> findByFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica);
}
