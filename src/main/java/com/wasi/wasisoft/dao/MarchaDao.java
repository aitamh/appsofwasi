package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Marcha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface MarchaDao extends JpaRepository<Marcha, Long> {

    List<Marcha> findByFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica);
}
