package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaAto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
public interface FichaAtoDao extends JpaRepository<FichaAto, Long> {

    List<FichaAto> findByIdexpediente(Expediente idexpediente);

}
