package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Marcha;
import com.wasi.wasisoft.model.MarchaPierna;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernande on 17/04/2018.
 */
public interface PiernaDerechaDao extends JpaRepository<MarchaPierna, Long>{

//    @Query("SELECT p FROM PiernaDerecha  p WHERE p.marcha")
    List<MarchaPierna> findAllByMarcha(Marcha marcha);
}
