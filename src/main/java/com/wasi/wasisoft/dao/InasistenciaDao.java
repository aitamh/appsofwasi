package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Inasistencia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InasistenciaDao extends JpaRepository<Inasistencia, Long>{


    List<Inasistencia> findAllByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);

}
