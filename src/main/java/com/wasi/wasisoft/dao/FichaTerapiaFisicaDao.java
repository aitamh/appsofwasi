package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface FichaTerapiaFisicaDao extends JpaRepository<FichaTerapiaFisica, Long> {

//    @Query(" SELECT  f.idcitaterapia FROM FichaTerapiaFisica f WHERE  f.estadoatencion = 0 ")
//    List<FichaTerapiaFisica> findAllPacientoNoAtendido();
//
    @Query("SELECT f.idexpediente FROM FichaTerapiaFisica f WHERE f.estadoatencion = 1")
    List<FichaTerapiaFisica> findAllPacienteAtendido();

    List<FichaTerapiaFisica> findByIdexpediente(Expediente idexpediente);

}
