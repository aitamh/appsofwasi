package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.DesarrolloSocial;
import com.wasi.wasisoft.model.FichaEducacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
public interface DesarrolloSocialDao extends JpaRepository<DesarrolloSocial, Long> {

    List<DesarrolloSocial> findByIdfichaeduc(FichaEducacion findByIdfichaeduc);

}
