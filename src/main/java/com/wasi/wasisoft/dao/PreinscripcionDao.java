package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Preinscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PreinscripcionDao extends JpaRepository<Preinscripcion, Long>{

    @Query("SELECT p FROM Preinscripcion p where p.estado=1")
    List<Preinscripcion> findAllDadosDeAlta();

    @Query("select  p from Preinscripcion p where p.estado=0")
    List<Preinscripcion> findAllDadosDeBaja();
}
