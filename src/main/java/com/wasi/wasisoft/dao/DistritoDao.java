package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Distrito;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface DistritoDao extends JpaRepository<Distrito, Long> {

    List<Distrito> findByIdprov(int id_prov);
}
