package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.apache.catalina.LifecycleState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnamnesisClinicoDao extends JpaRepository<AnamnesisClinico, Long> {

    List<AnamnesisClinico> findByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);
}
