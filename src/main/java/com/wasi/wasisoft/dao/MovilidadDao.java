package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Movilidad;
import com.wasi.wasisoft.model.MovilidadArticulaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MovilidadDao extends JpaRepository<Movilidad, Long>{


    List<Movilidad> findAllByIdmovarticulaciones(  MovilidadArticulaciones movilidadArticulaciones);

//    @Query("SELECT d FROM Cargo d WHERE d.departamentoArea.idDepartamentoArea =:idDepartamentoArea")
//    List<Cargo> findByCargoPorProyecto(@Param("idDepartamentoArea") Long idDepartamentoArea);

    @Query("SELECT m FROM Movilidad m WHERE m.numseguimiento = 1")
    List<Movilidad> getMovilidadSeguimientoUno();

    @Query("SELECT m FROM Movilidad m WHERE m.numseguimiento = 2")
    List<Movilidad> getMovilidadSeguimientoDos();

    @Query("SELECT m FROM Movilidad m WHERE m.numseguimiento = 3 ")
    List<Movilidad> getMovilidadSeguimientoTres();

//    @Query("SELECT m FROM Movilidad m where m.idmovarticulaciones = :idmovarticulaciones")
//    List<Movilidad> findAllByIdmovarticulaciones(Long idmovarticulaciones);

}
