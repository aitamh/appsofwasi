package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaPsicologia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
public interface FichaPsicologiaDao extends JpaRepository<FichaPsicologia, Long>{

    List<FichaPsicologia> findByIdexpediente(Expediente idexpediente);
}
