package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FichasocioeconDao extends JpaRepository<Fichasocioecon, Long> {

    List<Fichasocioecon> findByIdexpediente(Expediente idexpediente);

    @Query("SELECT fs.tipoFamilia FROM Fichasocioecon fs")
    List<Fichasocioecon> getPacientesByTipoFamilia();

    @Query("SELECT f.tipoSeguro FROM Fichasocioecon f WHERE f.tipoSeguro LIKE 'No tiene%'")
    List<Fichasocioecon> getPacienteByTipoSeguro();


}
