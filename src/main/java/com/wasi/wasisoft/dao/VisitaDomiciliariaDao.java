package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VisitaDomiciliariaDao extends JpaRepository<VisitaDomiciliaria,Long>{

    List<VisitaDomiciliaria> findByIdexpediente(Expediente idexpediente);
}
