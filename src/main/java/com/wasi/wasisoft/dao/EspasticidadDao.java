package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Espasticidad;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface EspasticidadDao extends JpaRepository<Espasticidad , Long> {

    List<Espasticidad> findByFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica);

}
