package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
public interface DiagnosticoFinalDao extends JpaRepository<DiagnosticoFinal, Long> {

    List<DiagnosticoFinal> findByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);
}
