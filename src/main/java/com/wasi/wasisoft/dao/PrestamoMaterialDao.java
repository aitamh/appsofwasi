package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.PrestamoMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface PrestamoMaterialDao extends JpaRepository<PrestamoMaterial, Long> {

   @Query("SELECT pm FROM PrestamoMaterial pm WHERE pm.tipooperacion=1")
    List<PrestamoMaterial> getMaterialPrestado();

   @Query("SELECT pm FROM PrestamoMaterial pm WHERE pm.tipooperacion=0")
    List<PrestamoMaterial> getMaterialVendido();

    List<PrestamoMaterial> findByIdexpediente(Expediente idexpediente);
}
