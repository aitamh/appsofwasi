package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.MedicamentoVenta;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
public interface MedicamentoVentaDao extends JpaRepository<MedicamentoVenta, Long> {


}
