package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaSalud;
import com.wasi.wasisoft.model.VisitaMedica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Nelly on 25/04/2018.
 */
public interface VisitaMedicaDao extends JpaRepository<VisitaMedica, Long> {

    List<VisitaMedica> findByIdfichasalud(FichaSalud fichaSalud);

}
