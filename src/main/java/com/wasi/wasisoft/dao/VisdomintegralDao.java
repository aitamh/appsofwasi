package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaSalud;
import com.wasi.wasisoft.model.Visdomintegral;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
public interface VisdomintegralDao extends JpaRepository<Visdomintegral, Long> {

    List<Visdomintegral> findByIdfichasalud(FichaSalud idfichasalud);

}
