package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.MaterialOrtopedico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialOrtopedicoDao extends JpaRepository<MaterialOrtopedico, Long>{
}
