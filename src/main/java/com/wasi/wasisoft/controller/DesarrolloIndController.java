package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.DesarrolloInd;
import com.wasi.wasisoft.service.DesarrolloIndService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class DesarrolloIndController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @Autowired
    protected DesarrolloIndService desarrolloIndService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateDesarrolloInd", method = RequestMethod.POST)
    public RestResponse saveOrUpdateDesarrolloInd(@RequestBody String desarrolloIndJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ desarrolloIndJson + "'");

        DesarrolloInd desarrolloInd = this.objectMapper.readValue(desarrolloIndJson, DesarrolloInd.class);
        this.desarrolloIndService.save(desarrolloInd);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/findByDesarrolloIndByIdanamnesis", method = RequestMethod.GET)
    public List<DesarrolloInd> findByDesarrolloIndByIdanamnesis(@RequestParam AnamnesisClinico idanamnesis){
        return this.desarrolloIndService.findDesarrolloIndByIdanamnesisclinico(idanamnesis);
    }

    @RequestMapping(value = "/deleteDesarrolloInd", method = RequestMethod.POST)
    public void deleteDesarrolloInd(@RequestBody String desarrolloIndJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        DesarrolloInd desarrolloInd = this.objectMapper.readValue(desarrolloIndJson, DesarrolloInd.class);

        if (desarrolloInd.getIddesainde() == null) {
            throw new Exception("El id esta nulo");
        }
        this.desarrolloIndService.deleteDesarrolloInd(desarrolloInd.getIddesainde());
    }
}
