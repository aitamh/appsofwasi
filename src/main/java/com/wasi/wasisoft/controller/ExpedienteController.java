package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Ubigeo;
import com.wasi.wasisoft.service.ExpedienteService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class ExpedienteController {

    @Autowired
    protected ExpedienteService expedienteService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateExpediente", method = RequestMethod.POST)
    public RestResponse saveOrUpdateExpediente(@RequestBody String expedienteJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        Expediente expediente = this.objectMapper.readValue(expedienteJson, Expediente.class);
        this.expedienteService.save(expediente);
        return new RestResponse(HttpStatus.OK.value(), "Expediente se guardo correctamente");
    }

    @RequestMapping(value = "/getExpediente", method = RequestMethod.GET)
    public List<Expediente> getExpediente() {
        return this.expedienteService.findAll();
    }


    @RequestMapping(value = "/getByCodigohistorial", method = RequestMethod.GET)
    public List<Expediente> findByCodigohistorial(@RequestParam String codigo){
        return  this.expedienteService.findByCodigohistorial(codigo);
    }

    @RequestMapping(value = "/deleteExpediente", method = RequestMethod.POST)
    public void deleteExpediente(@RequestBody String expedienteJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        Expediente expediente = this.objectMapper.readValue(expedienteJson, Expediente.class);
        if (expediente.getId_expediente() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.expedienteService.deleteExpediente(expediente.getId_expediente());
        }
    }
    
//--Busca el expediente por dni y retorna el expediente encontrado--
    @RequestMapping(value = "/getExpedienteByDni", method = RequestMethod.GET)
    public List<Expediente> getExpedienteByDni(@RequestParam int dni) {
        return this.expedienteService.findByDni(dni);
    }

// Retorna un array de pacientes por el genero
    @RequestMapping(value = "/getExpedienteBySexo", method = RequestMethod.GET)
    public List<Expediente> getExpedienteBySexo(){
        return this.expedienteService.findExpedienteBySexo();
    }

    private boolean validateEstado(Expediente expediente) {
        boolean isValid = true;

        if (expediente.getEstadoexpediente().equals(false)) {isValid = false;
        }
        return isValid;
    }
}