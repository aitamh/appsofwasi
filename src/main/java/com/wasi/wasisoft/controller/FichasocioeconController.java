package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.service.FichasocioeconService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sun.plugin2.message.transport.SerializingTransport;

import java.io.IOException;
import java.util.List;

@RestController
public class FichasocioeconController {

    @Autowired
    protected FichasocioeconService fichasocioeconService;

    @Autowired
    protected ObjectMapper objectMapper;
    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @RequestMapping(value = "/saveOrUpdateFichasocioecon", method = RequestMethod.POST)

    public RestResponse saveOrUpdateFichasocioecon(@RequestBody String jsonFichasocioecon)

            throws JsonParseException,JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ jsonFichasocioecon + "'");

        Fichasocioecon fichasocioecon = this.objectMapper.readValue(jsonFichasocioecon, Fichasocioecon.class);

        this.fichasocioeconService.save(fichasocioecon);

        return new RestResponse(HttpStatus.OK.value(), "La ficha socio economica se guardo correctamente");
    }

    @RequestMapping(value = "/getFichasocioecon", method = RequestMethod.GET)
    public List<Fichasocioecon> getFichasocioecon(){
        return this.fichasocioeconService.findAll();
    }

    @RequestMapping(value = "/getFichaSocioEconomicaByIdexpediente", method = RequestMethod.GET)
    List<Fichasocioecon> getFichaSocioEconomicaSegunIdExpediente(@RequestParam Expediente idexpediente){

        return this.fichasocioeconService.obtenerFichaSocioEconomica(idexpediente);
    }
//
//    @RequestMapping(value = "/getByCodigoHistoriaSocial", method = RequestMethod.GET)
//    public List<HistoriaSocial> getByCodigoHistoriaSocial(@RequestParam String codigo){
//        return  this.historiaSocialService.findByCodigoHistoria(codigo);
//    }

    @RequestMapping(value = "/deleteFichasocioecon", method = RequestMethod.POST)
    public void deleteFichasocioecon(@RequestBody String fichasocioeconJson) throws Exception{

        this.objectMapper = new ObjectMapper();

        Fichasocioecon fichasocioecon= this.objectMapper.readValue(fichasocioeconJson, Fichasocioecon.class);

        if (fichasocioecon.getId_socioecon() ==null){

            System.out.println("Registro de ficha socio economica no encontrado");

        }else {

            this.fichasocioeconService.deleteFichasocioecon(fichasocioecon.getId_socioecon());
        }
    }

}


