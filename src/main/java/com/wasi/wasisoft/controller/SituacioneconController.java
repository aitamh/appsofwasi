package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.service.SituacioneconService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class SituacioneconController {

    @Autowired
    protected SituacioneconService situacioneconService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/getSituacionecon", method = RequestMethod.GET)
    public List<Situacionecon> getSituacionecon(){
        return this.situacioneconService.findAllSituacioecon();
    }

    @RequestMapping(value = "/saveOrUpdateSituacionecon", method = RequestMethod.POST)
    public RestResponse saveOrUpdateSituacionecon(@RequestBody String situacioneconJson)
            throws JsonParseException,JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        Situacionecon situacionecon = this.objectMapper.readValue(situacioneconJson, Situacionecon.class);
        this.situacioneconService.save(situacionecon);
        return new RestResponse(HttpStatus.OK.value(), "La ficha socio economica se guardo correctamente");
    }

    @RequestMapping(value = "/getSituacioneconByIdexpediente", method = RequestMethod.GET)
    List<Situacionecon> getSituacioneconByIdexpediente(@RequestParam Expediente idexpediente){
        return this.situacioneconService.obtenerSituacionEconomica(idexpediente);
    }
}
