package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaPsicologia;
import com.wasi.wasisoft.service.FichaPsicologiaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@RestController
public class FichaPsicologiaController {

    @Autowired
    protected FichaPsicologiaService fichaPsicologiaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateFichaPsicologia")
    public RestResponse saveOrUpdateFichaPsicologia(@RequestBody String fichaPsicologiaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        FichaPsicologia fichaPsicologia = this.objectMapper.readValue(fichaPsicologiaJson, FichaPsicologia.class);
        this.fichaPsicologiaService.saveFichaPsicologia(fichaPsicologia);
        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteFichaPsicologia")
    public void deleteFichaPsicologia(@RequestBody String fichaPsicologiaJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        FichaPsicologia fichaPsicologia = this.objectMapper.readValue(fichaPsicologiaJson, FichaPsicologia.class);
        if (fichaPsicologia.getIdfichapsico() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.fichaPsicologiaService.deleteFichaPsicologia(fichaPsicologia.getIdfichapsico());
        }
    }

    @GetMapping("/getFichaPsicologiaPaciente")
    public List<FichaPsicologia> getFichaPsicologiaPaciente(@RequestParam Expediente idexpediente){
        return this.fichaPsicologiaService.getFichaPsicologiaPaciente(idexpediente);
    }
}
