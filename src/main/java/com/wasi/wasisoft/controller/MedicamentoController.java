package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Medicamento;
import com.wasi.wasisoft.model.MedicamentoVenta;
import com.wasi.wasisoft.service.MedicamentoService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.List;

@RestController
public class MedicamentoController {

    private static final Log LOGGER = LogFactory.getLog(MedicamentoController.class);
    @Autowired
    protected MedicamentoService medicamentoService;

    protected ObjectMapper mapper;

    @RequestMapping(value = "/saveOrUpdateMedicamento", method = RequestMethod.POST)
    public RestResponse saveOrUpdate(@RequestBody String medicamentoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.mapper = new ObjectMapper();

        Medicamento medicamento = this.mapper.readValue(medicamentoJson, Medicamento.class);
        LOGGER.info(" PARAMS: '"+ medicamentoJson + "'");
        this.medicamentoService.save(medicamento);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getAllMedicamento", method = RequestMethod.GET)
    public List<Medicamento> getAllMedicamento() {
        return this.medicamentoService.findAll();
    }

    @RequestMapping(value = "/deleteMedicamento", method = RequestMethod.POST)
    public void deleteMedicamento(@RequestBody String medicamentoJson) throws Exception {
        this.mapper = new ObjectMapper();

        Medicamento medicamento = this.mapper.readValue(medicamentoJson, Medicamento.class);

        if (medicamento.getIdMedicamento()== null) {
            throw new Exception("El id esta nulo");
        }
        this.medicamentoService.deleteMedicamento(medicamento.getIdMedicamento());
    }

    @PostMapping("/saveOrUpdateVentaMedicamento")
    public RestResponse saveOrUpdateVentaMedicamento(@RequestBody String medicamentoVentaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.mapper = new ObjectMapper();

        MedicamentoVenta medicamentoVenta = this.mapper.readValue(medicamentoVentaJson, MedicamentoVenta.class);
        LOGGER.info(" PARAMS: '"+ medicamentoVentaJson + "'");
        this.medicamentoService.saveMedicamentoVenta(medicamentoVenta);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @GetMapping("/getAllVentaMedicamento")
    public List<MedicamentoVenta> getAllVentaMedicamento() {
        return this.medicamentoService.getAllVentaMedicamento();
    }

    @PostMapping("/deleteVentaMedicamento")
    public void deleteVentaMedicamento(@RequestBody String medicamentoVentaJson) throws Exception {
        this.mapper = new ObjectMapper();

        MedicamentoVenta medicamentoVenta = this.mapper.readValue(medicamentoVentaJson, MedicamentoVenta.class);

        if (medicamentoVenta.getIdmedicventa()== null) {
            throw new Exception("El id esta nulo");
        }
        this.medicamentoService.deleteMedicamentoVenta(medicamentoVenta.getIdmedicventa());
    }
}
