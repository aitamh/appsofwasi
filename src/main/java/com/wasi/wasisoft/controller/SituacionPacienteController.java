package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionpaciente;
import com.wasi.wasisoft.service.SituacionpacienteService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class SituacionPacienteController {

    @Autowired
    protected SituacionpacienteService situacionpacienteService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping( value = "/saveOrUpdateSituacionPaciente", method = RequestMethod.POST)
    public RestResponse saveOrUpdateSituacionPaciente(@RequestBody String situacionpacienteJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Situacionpaciente situacionpaciente = this.objectMapper.readValue(situacionpacienteJson, Situacionpaciente.class);

        this.situacionpacienteService.save(situacionpaciente);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getSituacionPacienteByIdExpediente", method = RequestMethod.GET)
    public List<Situacionpaciente> getSituacionPacienteByIdExpediente(@RequestParam Expediente idexpediente){
        return situacionpacienteService.findByIdexpediente(idexpediente);
    }

    @RequestMapping(value = "/getExpedienteByPacienteActivo", method = RequestMethod.GET)
    public List<Situacionpaciente> getExpedienteByPacienteActivo(){
        return this.situacionpacienteService.findAllExpedienteByPacienteactivo();
    }

    @RequestMapping(value = "/getPacienteActivoAndAceptado", method = RequestMethod.GET)
    public List<Situacionpaciente> getPacienteActivoAndAceptado(){
        return this.situacionpacienteService.getPacienteActivoAndAceptado();
    }
}
