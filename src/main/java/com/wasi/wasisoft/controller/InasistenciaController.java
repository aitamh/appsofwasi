package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Inasistencia;
import com.wasi.wasisoft.service.InasistenciaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class InasistenciaController {

    @Autowired
    protected InasistenciaService inasistenciaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateInasistencia", method = RequestMethod.POST)
    public RestResponse saveOrUpdateInasistencia(@RequestBody String inasistenciaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Inasistencia inasistencia = this.objectMapper.readValue(inasistenciaJson, Inasistencia.class);

        this.inasistenciaService.saveInasistencia(inasistencia);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getInasistenciasByPacientes", method = RequestMethod.GET)
    public List<Inasistencia> getInasistencias(@RequestParam FichaTerapiaFisica idfichaterapiafisica){
        return this.inasistenciaService.findAllInasistencia(idfichaterapiafisica);
    }

    @RequestMapping(value = "/deleteInasistencias", method = RequestMethod.POST)
    public void deleteInasistencia(@RequestBody String inasistenciasJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Inasistencia inasistencia = this.objectMapper.readValue(inasistenciasJson, Inasistencia.class);

        if (inasistencia.getIdinasistencia() == null) {
            throw new Exception("El id esta nulo");
        }
        this.inasistenciaService.deleteInasistenecia(inasistencia.getIdinasistencia());
    }
}
