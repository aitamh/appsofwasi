package com.wasi.wasisoft.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Departamento;
import com.wasi.wasisoft.model.Distrito;
import com.wasi.wasisoft.model.Provincia;
import com.wasi.wasisoft.service.UbigeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UbigeoController {

    @Autowired
    protected UbigeoService ubigeoService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/getDepartamentos", method = RequestMethod.GET)
    public List<Departamento> getDepartamentos(){
        return this.ubigeoService.findAll();
    }

        @RequestMapping(value = "/getProvinciaByDepartamentos", method = RequestMethod.GET)
    public List<Provincia> getProvinciaByDepartamentos(@RequestParam int iddepa){
        return  this.ubigeoService.findByIddepa(iddepa);
    }

    @RequestMapping(value = "/getDistritoByProvincias", method = RequestMethod.GET)
    public List<Distrito> getDistritoByProvincias(@RequestParam int idprov){
        return  this.ubigeoService.findByIdprov(idprov);
    }


}
