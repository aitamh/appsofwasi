package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.AnamnesisClinicoService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController()
public class AnamnesisClinicoController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);
    @Autowired
    protected AnamnesisClinicoService anamnesisClinicoService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateAnamnesisClinico", method = RequestMethod.POST)

    public RestResponse saveOrUpdateAnamnesisClinico(@RequestBody String anamnesisClinicoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + anamnesisClinicoJson + "'");

        AnamnesisClinico anamnesisClinico = this.objectMapper.readValue(anamnesisClinicoJson, AnamnesisClinico.class);
        this.anamnesisClinicoService.save(anamnesisClinico);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/findAnamnesisClinicoByIdFichaTerapiaFisica", method = RequestMethod.GET)

    List<AnamnesisClinico> findAnamnesisClinicoByIdFichaTerapiaFisica(@RequestParam FichaTerapiaFisica idfichaterapiafisica) {
        return this.anamnesisClinicoService.findByAnamnesisClinicoByIdfichaterapiafisica(idfichaterapiafisica);
    }

    @RequestMapping(value = "/deleteAnamnesisClinico", method = RequestMethod.POST)
    public void deleteAnamnesisClinico(@RequestBody String anamnesisClinicoJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        AnamnesisClinico anamnesisClinico = this.objectMapper.readValue(anamnesisClinicoJson, AnamnesisClinico.class);

        if (anamnesisClinico.getIdanamnesis() == null) {
            throw new Exception("El id esta nulo");
        }
        this.anamnesisClinicoService.delete(anamnesisClinico.getIdanamnesis());
    }
}