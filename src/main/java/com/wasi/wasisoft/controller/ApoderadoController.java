package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Apoderado;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.service.ApoderadoService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class ApoderadoController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);
    @Autowired
    protected ApoderadoService apoderadoService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateApoderado", method = RequestMethod.POST)
    public RestResponse saveOrUpdateApoderado(@RequestBody String apoderadoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        LOGGER.info(" PARAMS: '"+ apoderadoJson + "'");
        Apoderado apoderado = this.objectMapper.readValue(apoderadoJson, Apoderado.class);
        this.apoderadoService.save(apoderado);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getApoderado", method = RequestMethod.GET)
    public List<Apoderado> getApoderado() {
        return this.apoderadoService.findAll();
    }

    @RequestMapping(value = "/deleteApoderado", method = RequestMethod.POST)
    public void deleteApoderado(@RequestBody String apoderadoJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Apoderado apoderado = this.objectMapper.readValue(apoderadoJson, Apoderado.class);

        if (apoderado.getId_apoderado() == null) {
            throw new Exception("El id esta nulo");
        }
        this.apoderadoService.deleteApoderado(apoderado.getId_apoderado());
    }

    @RequestMapping(value = "/getApoderadoByIdExpediente", method = RequestMethod.GET)
    public List<Apoderado> getApoderadoByIdExpediente(@RequestParam Expediente idexpediente) {
        return this.apoderadoService.findByIdexpediente(idexpediente);
    }
}
