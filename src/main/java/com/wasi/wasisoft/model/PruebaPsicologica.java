package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@Entity
@Table (name = "pruepsico")
public class PruebaPsicologica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pruepsico")
    private Integer idPruepsico;

    @Size(max = 100)
    @Column(name = "nombre_prueba")
    private String nombrePrueba;

    @Size(max = 100)
    @Column(name = "diagnostico")
    private String diagnostico;

    @Size(max = 200)
    @Column(name = "obser_conductual")
    private String obserConductual;

    @Size(max = 200)
    @Column(name = "resultados")
    private String resultados;

    @Column(name = "fecha_prueba")
    @Temporal(TemporalType.DATE)
    private Date fechaPrueba;

    @Size(max = 15)
    @Column(name = "nivelpsicomotor")
    private String nivelpsicomotor;

    @Size(max = 15)
    @Column(name = "nivelintelectual")
    private String nivelintelectual;

    @Size(max = 15)
    @Column(name = "nivelmaduracion")
    private String nivelmaduracion;

    @Size(max = 10)
    @Column(name = "probconductuales")
    private String probconductuales;

    @Size(max = 10)
    @Column(name = "probemocionales")
    private String probemocionales;

    @Size(max = 20)
    @Column(name = "procesoduelomadre")
    private String procesoduelomadre;

    @Size(max = 20)
    @Column(name = "relacionmadrehijo")
    private String relacionmadrehijo;

    @Size(max = 100)
    @Column(name = "rasgopersonalidad")
    private String rasgopersonalidad;

    @Size(max = 50)
    @Column(name = "depresionmadre")
    private String depresionmadre;

    @Size(max = 20)
    @Column(name = "procesoduelopadre")
    private String procesoduelopadre;

    @Size(max = 100)
    @Column(name = "personalidadpadre")
    private String personalidadpadre;

    @Size(max = 50)
    @Column(name = "depresionpadre")
    private String depresionpadre;

    @Size(max = 300)
    @Column(name = "intervencion")
    private String intervencion;

    @Size(max = 20)
    @Column(name = "tipopaciente")
    private String tipopaciente;

    @JoinColumn(name = "id_instrevaluacion", referencedColumnName = "id_instrevaluacion")
    @ManyToOne
    private InstrEvaluacion idinstrevaluacion;

    @JoinColumn(name = "id_fichapsico", referencedColumnName = "id_fichapsico")
    @ManyToOne
    private FichaPsicologia idfichapsicologia;


}
