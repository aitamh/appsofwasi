package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
@Entity
@Table (name = "resultpruebas")
public class ResultadoPruebas implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_resultpruebas")
    private Long idresultpruebas;

    @Size(min = 1, max = 100)
    @Column(name = "diagnostico")
    private String diagnostico;

    @Size(min = 1, max = 100)
    @Column(name = "observacion")
    private String observacion;

    @Column(name = "resultados")
    private String resultados;

    //obtener el nombre de los intrumentos de evaluacion (tabla InstrEvaluacion)
    @JoinColumn(name = "id_instrevaluacion", referencedColumnName = "id_instrevaluacion")
    @ManyToOne
    private InstrEvaluacion instrEvaluacion;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente idexpediente;

    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    @ManyToOne
    private Area idarea;

    public ResultadoPruebas() {
    }

    public ResultadoPruebas(Long idresultpruebas) {
        this.idresultpruebas = idresultpruebas;
    }

    public ResultadoPruebas(String diagnostico, String observacion, String resultados, InstrEvaluacion instrEvaluacion, Expediente idexpediente, Area idarea) {
        this.diagnostico = diagnostico;
        this.observacion = observacion;
        this.resultados = resultados;
        this.instrEvaluacion = instrEvaluacion;
        this.idexpediente = idexpediente;
        this.idarea = idarea;
    }

    public Long getIdresultpruebas() {
        return idresultpruebas;
    }

    public void setIdresultpruebas(Long idresultpruebas) {
        this.idresultpruebas = idresultpruebas;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getResultados() {
        return resultados;
    }

    public void setResultados(String resultados) {
        this.resultados = resultados;
    }

    public InstrEvaluacion getInstrEvaluacion() {
        return instrEvaluacion;
    }

    public void setInstrEvaluacion(InstrEvaluacion instrEvaluacion) {
        this.instrEvaluacion = instrEvaluacion;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Area getIdarea() {
        return idarea;
    }

    public void setIdarea(Area idarea) {
        this.idarea = idarea;
    }
}
