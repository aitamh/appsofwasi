package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "refexterna")
public class ReferenciaExterna implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idrefexterna")
    private Long idrefexterna;

    @Column(name = "solicitud_ayuda")
    private String solicitud_ayuda;

    @Column(name = "derivacion")
    private String derivacion;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public ReferenciaExterna() {
    }

    public ReferenciaExterna(String solicitud_ayuda, String derivacion, Expediente idexpediente) {
        this.solicitud_ayuda = solicitud_ayuda;
        this.derivacion = derivacion;
        this.idexpediente = idexpediente;
    }

    public Long getIdrefexterna() {
        return idrefexterna;
    }

    public void setIdrefexterna(Long idrefexterna) {
        this.idrefexterna = idrefexterna;
    }

    public String getSolicitud_ayuda() {
        return solicitud_ayuda;
    }

    public void setSolicitud_ayuda(String solicitud_ayuda) {
        this.solicitud_ayuda = solicitud_ayuda;
    }

    public String getDerivacion() {
        return derivacion;
    }

    public void setDerivacion(String derivacion) {
        this.derivacion = derivacion;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
