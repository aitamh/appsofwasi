package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "citaterapia")
@Access(AccessType.FIELD)
public class CitaTerapia extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column(name = "id_citaterapia", unique = true, nullable = false)
    private Long idcitaterapia;

    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "horainicio")
    private String horainicio;
    @Column(name = "horafin")
    private String horafin;

    @Size(max = 100)
    @Column(name = "motivo_cita")
    private String motivoCita;

    @Column(name = "atencionpaciente")
    private Boolean atencionpaciente;

    @Column(name = "coloragenda")
    private String color;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    @JoinColumn(name = "id_personal", referencedColumnName = "id_personal")
    @ManyToOne
    private Personal idpersonal;

    public CitaTerapia() {
    }

    public CitaTerapia(Long idcitaterapia) {
        this.idcitaterapia = idcitaterapia;
    }

    public CitaTerapia(Date fecha, String horainicio, String horafin, String motivoCita, Boolean atencionpaciente, String color, Expediente idexpediente, Personal idpersonal) {
        this.fecha = fecha;
        this.horainicio = horainicio;
        this.horafin = horafin;
        this.motivoCita = motivoCita;
        this.atencionpaciente = atencionpaciente;
        this.color = color;
        this.idexpediente = idexpediente;
        this.idpersonal = idpersonal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(String horainicio) {
        this.horainicio = horainicio;
    }

    public String getHorafin() {
        return horafin;
    }

    public void setHorafin(String horafin) {
        this.horafin = horafin;
    }

    public String getMotivoCita() {
        return motivoCita;
    }

    public Long getIdcitaterapia() {
        return idcitaterapia;
    }

    public void setIdcitaterapia(Long idcitaterapia) {
        this.idcitaterapia = idcitaterapia;
    }

    public void setMotivoCita(String motivoCita) {
        this.motivoCita = motivoCita;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Personal getIdpersonal() {
        return idpersonal;
    }

    public void setIdpersonal(Personal idpersonal) {
        this.idpersonal = idpersonal;
    }

    public Boolean getAtencionpaciente() {
        return atencionpaciente;
    }

    public void setAtencionpaciente(Boolean atencionpaciente) {
        this.atencionpaciente = atencionpaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcitaterapia != null ? idcitaterapia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CitaTerapia)) {
            return false;
        }
        CitaTerapia other = (CitaTerapia) object;
        if ((this.idcitaterapia == null && other.idcitaterapia != null) || (this.idcitaterapia != null && !this.idcitaterapia.equals(other.idcitaterapia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wasi.wasisoft.model.Citaterapia[ idCitaterapia=" + idcitaterapia + " ]";
    }
}
