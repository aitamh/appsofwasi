package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 09/05/2018.
 */
@Entity
@Table(name = "evalsemestral")
public class EvalSemestral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_evalsemestral")
    private Long idevalsemestral;

    // identifica el numero de evaluacion (0=primera evaluacion, 1=segundaevaluacion)
    @Column(name = "numevaluacion")
    private Integer numevaluacion;

    //idenetifica el tipo de habilidad(1=percepcion_comunicacion, 2=expresion, 3=socializacion, 4=percepcion aprestamiento, etc)
    @Column(name = "tipohabilidad")
    private Integer tipohabilidad;

    @Column(name = "item1")
    private  Integer  item1;
    @Column(name = "item2")
    private  Integer  item2;
    @Column(name = "item3")
    private  Integer  item3;
    @Column(name = "item4")
    private  Integer  item4;
    @Column(name = "item5")
    private  Integer  item5;
    @Column(name = "item6")
    private  Integer  item6;
    @Column(name = "item7")
    private  Integer item7;
    @Column(name = "item8")
    private  Integer  item8;
    @Column(name = "item9")
    private  Integer  item9;
    @Column(name = "item10")
    private  Integer  item10;
    @Column(name = "item11")
    private  Integer  item11;
    @Column(name = "item12")
    private  Integer  item12;
    @Column(name = "item13")
    private  Integer  item13;
    @Column(name = "item14")
    private  Integer  item14;
    @Column(name = "item15")
    private  Integer  item15;
    @Column(name = "item16")
    private  Integer item16;
    @Column(name = "item17")
    private  Integer  item17;
    @Column(name = "item18")
    private  Integer  item18;

    //Observaciones
    @Column(name = "observacion1")
    private String observacion1;
    @Column(name = "observacion2")
    private String observacion2;
    @Column(name = "observacion3")
    private String observacion3;
    @Column(name = "observacion4")
    private String observacion4;
    @Column(name = "observacion5")
    private String observacion5;
    @Column(name = "observacion6")
    private String observacion6;
    @Column(name = "observacion7")
    private String observacion7;
    @Column(name = "observacion8")
    private String observacion8;
    @Column(name = "observacion9")
    private String observacion9;
    @Column(name = "observacion10")
    private String observacion10;
    @Column(name = "observacion11")
    private String observacion11;
    @Column(name = "observacion12")
    private String observacion12;
    @Column(name = "observacion13")
    private String observacion13;
    @Column(name = "observacion14")
    private String observacion14;
    @Column(name = "observacion15")
    private String observacion15;
    @Column(name = "observacion16")
    private String observacion16;
    @Column(name = "observacion17")
    private String observacion17;
    @Column(name = "observacion18")
    private String observacion18;

    @Column(name = "obsinicial")
    private String obsinicial;
    @Column(name = "obsfinal")
    private String obsfinal;

    @JoinColumn(name = "id_fichaeduc", referencedColumnName = "id_fichaeduc")
    @ManyToOne
    private FichaEducacion idfichaeducacion;

    public EvalSemestral() {
    }

    public EvalSemestral(Long idevalsemestral) {
        this.idevalsemestral = idevalsemestral;
    }

    public EvalSemestral(Integer numevaluacion, Integer tipohabilidad, Integer item1, Integer item2, Integer item3, Integer item4, Integer item5, Integer item6, Integer item7, Integer item8, Integer item9, Integer item10, Integer item11, Integer item12, Integer item13, Integer item14, Integer item15, Integer item16, Integer item17, Integer item18, String observacion1, String observacion2, String observacion3, String observacion4, String observacion5, String observacion6, String observacion7, String observacion8, String observacion9, String observacion10, String observacion11, String observacion12, String observacion13, String observacion14, String observacion15, String observacion16, String observacion17, String observacion18, String obsinicial, String obsfinal,FichaEducacion idfichaeducacion) {
        this.numevaluacion = numevaluacion;
        this.tipohabilidad = tipohabilidad;
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;
        this.item5 = item5;
        this.item6 = item6;
        this.item7 = item7;
        this.item8 = item8;
        this.item9 = item9;
        this.item10 = item10;
        this.item11 = item11;
        this.item12 = item12;
        this.item13 = item13;
        this.item14 = item14;
        this.item15 = item15;
        this.item16 = item16;
        this.item17 = item17;
        this.item18 = item18;
        this.observacion1 = observacion1;
        this.observacion2 = observacion2;
        this.observacion3 = observacion3;
        this.observacion4 = observacion4;
        this.observacion5 = observacion5;
        this.observacion6 = observacion6;
        this.observacion7 = observacion7;
        this.observacion8 = observacion8;
        this.observacion9 = observacion9;
        this.observacion10 = observacion10;
        this.observacion11 = observacion11;
        this.observacion12 = observacion12;
        this.observacion13 = observacion13;
        this.observacion14 = observacion14;
        this.observacion15 = observacion15;
        this.observacion16 = observacion16;
        this.observacion17 = observacion17;
        this.observacion18 = observacion18;
        this.obsinicial = obsinicial;
        this.obsfinal = obsfinal;
        this.idfichaeducacion = idfichaeducacion;
    }

    public void setItem11(Integer item11) {
        this.item11 = item11;
    }

    public FichaEducacion getIdfichaeducacion() {
        return idfichaeducacion;
    }

    public void setIdfichaeducacion(FichaEducacion idfichaeducacion) {
        this.idfichaeducacion = idfichaeducacion;
    }

    public Long getIdevalsemestral() {
        return idevalsemestral;
    }

    public void setIdevalsemestral(Long idevalsemestral) {
        this.idevalsemestral = idevalsemestral;
    }

    public Integer getNumevaluacion() {
        return numevaluacion;
    }

    public void setNumevaluacion(Integer numevaluacion) {
        this.numevaluacion = numevaluacion;
    }

    public Integer getTipohabilidad() {
        return tipohabilidad;
    }

    public void setTipohabilidad(Integer tipohabilidad) {
        this.tipohabilidad = tipohabilidad;
    }

    public Integer getItem1() {
        return item1;
    }

    public void setItem1(Integer item1) {
        this.item1 = item1;
    }

    public Integer getItem2() {
        return item2;
    }

    public void setItem2(Integer item2) {
        this.item2 = item2;
    }

    public Integer getItem3() {
        return item3;
    }

    public void setItem3(Integer item3) {
        this.item3 = item3;
    }

    public Integer getItem4() {
        return item4;
    }

    public void setItem4(Integer item4) {
        this.item4 = item4;
    }

    public Integer getItem5() {
        return item5;
    }

    public void setItem5(Integer item5) {
        this.item5 = item5;
    }

    public Integer getItem6() {
        return item6;
    }

    public void setItem6(Integer item6) {
        this.item6 = item6;
    }

    public Integer getItem7() {
        return item7;
    }

    public void setItem7(Integer item7) {
        this.item7 = item7;
    }

    public Integer getItem8() {
        return item8;
    }

    public void setItem8(Integer item8) {
        this.item8 = item8;
    }

    public Integer getItem9() {
        return item9;
    }

    public void setItem9(Integer item9) {
        this.item9 = item9;
    }

    public Integer getItem10() {
        return item10;
    }

    public void setItem10(Integer item10) {
        this.item10 = item10;
    }

    public Integer getItem11() {
        return item11;
    }

    public void setIte11(Integer item11) {
        this.item11 = item11;
    }

    public Integer getItem12() {
        return item12;
    }

    public void setItem12(Integer item12) {
        this.item12 = item12;
    }

    public Integer getItem13() {
        return item13;
    }

    public void setItem13(Integer item13) {
        this.item13 = item13;
    }

    public Integer getItem14() {
        return item14;
    }

    public void setItem14(Integer item14) {
        this.item14 = item14;
    }

    public Integer getItem15() {
        return item15;
    }

    public void setItem15(Integer item15) {
        this.item15 = item15;
    }

    public Integer getItem16() {
        return item16;
    }

    public void setItem16(Integer item16) {
        this.item16 = item16;
    }

    public Integer getItem17() {
        return item17;
    }

    public void setItem17(Integer item17) {
        this.item17 = item17;
    }

    public Integer getItem18() {
        return item18;
    }

    public void setItem18(Integer item18) {
        this.item18 = item18;
    }

    public String getObservacion1() {
        return observacion1;
    }

    public void setObservacion1(String observacion1) {
        this.observacion1 = observacion1;
    }

    public String getObservacion2() {
        return observacion2;
    }

    public void setObservacion2(String observacion2) {
        this.observacion2 = observacion2;
    }

    public String getObservacion3() {
        return observacion3;
    }

    public void setObservacion3(String observacion3) {
        this.observacion3 = observacion3;
    }

    public String getObservacion4() {
        return observacion4;
    }

    public void setObservacion4(String observacion4) {
        this.observacion4 = observacion4;
    }

    public String getObservacion5() {
        return observacion5;
    }

    public void setObservacion5(String observacion5) {
        this.observacion5 = observacion5;
    }

    public String getObservacion6() {
        return observacion6;
    }

    public void setObservacion6(String observacion6) {
        this.observacion6 = observacion6;
    }

    public String getObservacion7() {
        return observacion7;
    }

    public void setObservacion7(String observacion7) {
        this.observacion7 = observacion7;
    }

    public String getObservacion8() {
        return observacion8;
    }

    public void setObservacion8(String observacion8) {
        this.observacion8 = observacion8;
    }

    public String getObservacion9() {
        return observacion9;
    }

    public void setObservacion9(String observacion9) {
        this.observacion9 = observacion9;
    }

    public String getObservacion10() {
        return observacion10;
    }

    public void setObservacion10(String observacion10) {
        this.observacion10 = observacion10;
    }

    public String getObservacion11() {
        return observacion11;
    }

    public void setObservacion11(String observacion11) {
        this.observacion11 = observacion11;
    }

    public String getObservacion12() {
        return observacion12;
    }

    public void setObservacion12(String observacion12) {
        this.observacion12 = observacion12;
    }

    public String getObservacion13() {
        return observacion13;
    }

    public void setObservacion13(String observacion13) {
        this.observacion13 = observacion13;
    }

    public String getObservacion14() {
        return observacion14;
    }

    public void setObservacion14(String observacion14) {
        this.observacion14 = observacion14;
    }

    public String getObservacion15() {
        return observacion15;
    }

    public void setObservacion15(String observacion15) {
        this.observacion15 = observacion15;
    }

    public String getObservacion16() {
        return observacion16;
    }

    public void setObservacion16(String observacion16) {
        this.observacion16 = observacion16;
    }

    public String getObservacion17() {
        return observacion17;
    }

    public void setObservacion17(String observacion17) {
        this.observacion17 = observacion17;
    }

    public String getObservacion18() {
        return observacion18;
    }

    public void setObservacion18(String observacion18) {
        this.observacion18 = observacion18;
    }

    public String getObsinicial() {
        return obsinicial;
    }

    public void setObsinicial(String obsinicial) {
        this.obsinicial = obsinicial;
    }

    public String getObsfinal() {
        return obsfinal;
    }

    public void setObsfinal(String obsfinal) {
        this.obsfinal = obsfinal;
    }
}
