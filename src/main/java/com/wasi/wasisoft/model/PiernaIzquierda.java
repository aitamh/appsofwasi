package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
@Entity
@Table(name = "piernaizquierda")
public class PiernaIzquierda {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_piernaizquierda")
    private Long idpiernaizquierda;

    @Column(name = "fsegpiernaiz")
    private Date fsegpiernaiz;

    @Column(name = "numsegpiernaiz")
    private Integer numsegpiernaiz;
    /**
     * Datos de fase de apoyo ( pf = plano frontal, ps = planosagital, ap=apoyo, imp= impulsion)
     */
    @Column(name = "troncoincap")
    private String troncoincap;
    @Column(name = "troncorotap")
    private String troncorotapyo;
    @Column(name = "pelvissubeap")
    private String pelvissubeap;
    @Column(name = "pelviscontraap")
    private String pelviscontraap;
    @Column(name = "caderarotap")
    private String caderarotap;
    @Column(name = "caderaabduap")
    private String caderaabduap;
    @Column(name = "pstroncoap")
    private String pstroncoap;
    @Column(name = "pspelvisap")
    private String pspelvisap;
    @Column(name = "pscaderaap")
    private String pscaderaap;
    @Column(name = "psrodillflexap")
    private String psrodillflexap;
    @Column(name = "psrodillhiperap")
    private String psrodillhiperap;
    @Column(name = "pstobillantpieap")
    private String pstobillantpieap;
    @Column(name = "pstobillpieap")
    private String pstobillpieap;
    @Column(name = "pstobilldedosap")
    private String pstobilldedosap;
    /**
     * Datos de la fase impulsion(imp)
     */
    @Column(name = "pftroncoincimp")
    private String pftroncoincimp;
    @Column(name = "pftroncorotimp")
    private String pftroncorotimp;
    @Column(name = "pfpelvissubeimp")
    private String pfpelvissubeimp;
    @Column(name = "pfpelviscontraimp")
    private String pfpelviscontraimp;
    @Column(name = "pfcaderarotimp")
    private String pfcaderarotimp;
    @Column(name = "pfcaderaabduimp")
    private String pfcaderaabduimp;
    @Column(name = "pstroncoimp")
    private String pstroncoapimp;
    @Column(name = "pspelvisimp")
    private String pspelvisimp;
    @Column(name = "pscaderaimp")
    private String pscaderaimp;
    @Column(name = "psrodillfleximp")
    private String psrodillfleximp;
    @Column(name = "psrodillhiperimp")
    private String psrodillhiperimp;
    @Column(name = "pstobillntepieimp")
    private String pstobillantepieimp;
    @Column(name = "pstobillpieimp")
    private String pstobillpieimp;
    @Column(name = "pstobilldedosimp")
    private String pstobilldedosimp;

    @JoinColumn(name = "id_marcha", referencedColumnName = "id_marcha")
    @ManyToOne
    private Marcha marcha;


}
