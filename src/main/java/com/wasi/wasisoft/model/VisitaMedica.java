package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@Entity
@Table(name = "visitamedica")
public class VisitaMedica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_visitamedica")
    private Long idvisitamedica;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "motivo_visita")
    private String motivovisita;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_visita")
    @Temporal(TemporalType.DATE)
    private Date fechavisita;

    @Size(max = 100)
    @Column(name = "estab_salud")
    private String estabsalud;

    @Size(max = 30)
    @Column(name = "medico_tratante")
    private String medicotratante;

    @Size(max = 300)
    @Column(name = "exam_indicados")
    private String examindicados;

    @Size(max = 300)
    @Column(name = "resultados")
    private String resultados;

    @Size(max = 400)
    @Column(name = "tratamiento")
    private String tratamiento;

    @Size(max = 200)
    @Column(name = "otros")
    private String otros;

    @JoinColumn(name = "id_fichasalud", referencedColumnName = "id_fichasalud")
    @ManyToOne
    private FichaSalud idfichasalud;

    public VisitaMedica() {
    }

    public VisitaMedica(Long idvisitamedica) {
        this.idvisitamedica = idvisitamedica;
    }

    public VisitaMedica(String motivovisita, Date fechavisita, String estabsalud, String medicotratante, String examindicados, String resultados, String tratamiento, String otros, FichaSalud idfichasalud) {
        this.motivovisita = motivovisita;
        this.fechavisita = fechavisita;
        this.estabsalud = estabsalud;
        this.medicotratante = medicotratante;
        this.examindicados = examindicados;
        this.resultados = resultados;
        this.tratamiento = tratamiento;
        this.otros = otros;
        this.idfichasalud = idfichasalud;
    }

    public Long getIdvisitamedica() {
        return idvisitamedica;
    }

    public void setIdvisitamedica(Long idvisitamedica) {
        this.idvisitamedica = idvisitamedica;
    }

    public String getMotivovisita() {
        return motivovisita;
    }

    public void setMotivovisita(String motivovisita) {
        this.motivovisita = motivovisita;
    }

    public Date getFechavisita() {
        return fechavisita;
    }

    public void setFechavisita(Date fechavisita) {
        this.fechavisita = fechavisita;
    }

    public String getEstabsalud() {
        return estabsalud;
    }

    public void setEstabsalud(String estabsalud) {
        this.estabsalud = estabsalud;
    }

    public String getMedicotratante() {
        return medicotratante;
    }

    public void setMedicotratante(String medicotratante) {
        this.medicotratante = medicotratante;
    }

    public String getExamindicados() {
        return examindicados;
    }

    public void setExamindicados(String examindicados) {
        this.examindicados = examindicados;
    }

    public String getResultados() {
        return resultados;
    }

    public void setResultados(String resultados) {
        this.resultados = resultados;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public FichaSalud getIdfichasalud() {
        return idfichasalud;
    }

    public void setIdfichasalud(FichaSalud idfichasalud) {
        this.idfichasalud = idfichasalud;
    }
}