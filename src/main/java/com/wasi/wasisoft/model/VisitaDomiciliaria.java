package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "visitadomiciliaria")
@Access(AccessType.FIELD)
public class VisitaDomiciliaria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_visitadomiciliaria")
    private Long id_visitadomiciliaria;
    @Basic(optional = false)
     @Column(name = "motivovisita")
    private String motivovisita;
    @Basic(optional = false)

    @Column(name = "fechavisita")
    @Temporal(TemporalType.DATE)
    private Date fechavisita;

    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "conclusionesvisita")
    private String conclusionesvisita;
    @Lob
    @Column(name = "evidenciavisita")
    private byte[] evidenciavisita;

    @Column(name = "entrevistado")
    private String entrevistado;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public VisitaDomiciliaria() {
    }

    public VisitaDomiciliaria(String motivovisita, Date fechavisita, String conclusionesvisita, byte[] evidenciavisita, String entrevistado, Expediente idexpediente) {
        this.motivovisita = motivovisita;
        this.fechavisita = fechavisita;
        this.conclusionesvisita = conclusionesvisita;
        this.evidenciavisita = evidenciavisita;
        this.entrevistado = entrevistado;
        this.idexpediente = idexpediente;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Long getId_visitadomiciliaria() {
        return id_visitadomiciliaria;
    }

    public void setId_visitadomiciliaria(Long id_visitadomiciliaria) {
        this.id_visitadomiciliaria = id_visitadomiciliaria;
    }

    public String getMotivovisita() {
        return motivovisita;
    }

    public void setMotivovisita(String motivovisita) {
        this.motivovisita = motivovisita;
    }

    public Date getFechavisita() {
        return fechavisita;
    }

    public void setFechavisita(Date fechavisita) {
        this.fechavisita = fechavisita;
    }

    public String getConclusionesvisita() {
        return conclusionesvisita;
    }

    public void setConclusionesvisita(String conclusionesvisita) {
        this.conclusionesvisita = conclusionesvisita;
    }

    public byte[] getEvidenciavisita() {
        return evidenciavisita;
    }

    public void setEvidenciavisita(byte[] evidenciavisita) {
        this.evidenciavisita = evidenciavisita;
    }

    public VisitaDomiciliaria(Long id_visitadomiciliaria) {
        this.id_visitadomiciliaria = id_visitadomiciliaria;
    }

    public String getEntrevistado() {
        return entrevistado;
    }

    public void setEntrevistado(String entrevistado) {
        this.entrevistado = entrevistado;
    }

}
