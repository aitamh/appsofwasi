package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "regispres")
public class PrestamoMaterial {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registroprestamo")
    private Long idprestamomaterial;

    @Column(name = "tipooperacion")
    private Boolean tipooperacion;

    @Column(name = "finiciooperacion")
    private Date finiciooperacion;

    @Column(name = "ffinprestamo")
    private Date ffinprestamo;

    @Column(name = "condicionprestamo")
    private String condicionprestamo;

    @Column(name = "monto")
    private Double monto;

    @Column(name = "estado_devolucion")
    private Boolean estado_devolucion;

    @JoinColumn(name = "id_materialortopedico", referencedColumnName = "id_materialortopedico")
    @ManyToOne
    private MaterialOrtopedico idmaterialortopedico;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public PrestamoMaterial() {
    }

    public PrestamoMaterial(Long i) {
    }

    public PrestamoMaterial(Boolean tipooperacion, Date finiciooperacion, Date ffinprestamo, String condicionprestamo, Double monto, Boolean estado_devolucion, MaterialOrtopedico idmaterialortopedico, Expediente idexpediente) {
        this.tipooperacion = tipooperacion;
        this.finiciooperacion = finiciooperacion;
        this.ffinprestamo = ffinprestamo;
        this.condicionprestamo = condicionprestamo;
        this.monto = monto;
        this.estado_devolucion = estado_devolucion;
        this.idmaterialortopedico = idmaterialortopedico;
        this.idexpediente = idexpediente;
    }

    public Long getIdprestamomaterial() {
        return idprestamomaterial;
    }

    public void setIdregistroprestamo(Long idprestamomaterial) {
        this.idprestamomaterial = idprestamomaterial;
    }

    public Boolean getTipooperacion() {
        return tipooperacion;
    }

    public void setTipooperacion(Boolean tipooperacion) {
        this.tipooperacion = tipooperacion;
    }

    public Date getFiniciooperacion() {
        return finiciooperacion;
    }

    public void setFiniciooperacion(Date finiciooperacion) {
        this.finiciooperacion = finiciooperacion;
    }

    public Date getFfinprestamo() {
        return ffinprestamo;
    }

    public void setFfinprestamo(Date ffinprestamo) {
        this.ffinprestamo = ffinprestamo;
    }

    public String getCondicionprestamo() {
        return condicionprestamo;
    }

    public void setCondicionprestamo(String condicionprestamo) {
        this.condicionprestamo = condicionprestamo;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Boolean getEstado_devolucion() {
        return estado_devolucion;
    }

    public void setEstado_devolucion(Boolean estado_devolucion) {
        this.estado_devolucion = estado_devolucion;
    }

    public MaterialOrtopedico getIdmaterialortopedico() {
        return idmaterialortopedico;
    }

    public void setIdmaterialortopedico(MaterialOrtopedico idmaterialortopedico) {
        this.idmaterialortopedico = idmaterialortopedico;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
