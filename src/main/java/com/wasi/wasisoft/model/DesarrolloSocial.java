package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
@Entity
@Table (name = "desarrollosocial")
public class DesarrolloSocial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_desarrollosocial")
    private Long iddesarrollosocial;

    @Size(max = 10)
    @Column(name = "rel_espontanea")
    private String relespontanea;
    @Size(max = 10)
    @Column(name = "expli_comporta")
    private String explicomporta;
    @Size(max = 10)
    @Column(name = "part_actgrup")
    private String partactgrup;
    @Size(max = 10)
    @Column(name = "trab_indiv")
    private String trabindiv;
    @Size(max = 10)
    @Column(name = "lengua_ecolalico")
    private String lenguaecolalico;
    @Size(max = 10)
    @Column(name = "dific_adapt")
    private String dificadapt;
    @Size(max = 10)
    @Column(name = "es_colaborador")
    private String escolaborador;
    @Size(max = 10)
    @Column(name = "normas_sociales")
    private String normassociales;
    @Size(max = 10)
    @Column(name = "normas_escolar")
    private String normasescolar;
    @Size(max = 10)
    @Column(name = "humor")
    private String humor;
    @Size(max = 10)
    @Column(name = "mov_estereot")
    private String movestereot;
    @Size(max = 10)
    @Column(name = "pataletas")
    private String pataletas;
    @Size(max = 10)
    @Column(name = "reaccion_luces")
    private String reaccionluces;
    @Size(max = 10)
    @Column(name = "reaccion_sonido")
    private String reaccionsonido;
    @Size(max = 10)
    @Column(name = "reaccion_persext")
    private String reaccionpersext;
    @Size(max = 200)
    @Column(name = "obs_desocial")
    private String obsdesocial;

    @JoinColumn(name = "id_fichaeduc", referencedColumnName = "id_fichaeduc")
    @ManyToOne
    private FichaEducacion idfichaeduc;

    public DesarrolloSocial() {
    }

    public DesarrolloSocial(Long iddesarrollosocial) {
        this.iddesarrollosocial = iddesarrollosocial;
    }

    public DesarrolloSocial(String relespontanea, String explicomporta, String partactgrup, String trabindiv, String lenguaecolalico, String dificadapt, String escolaborador, String normassociales, String normasescolar, String humor, String movestereot, String pataletas, String reaccionluces, String reaccionsonido, String reaccionpersext, String obsdesocial, FichaEducacion idfichaeduc) {
        this.relespontanea = relespontanea;
        this.explicomporta = explicomporta;
        this.partactgrup = partactgrup;
        this.trabindiv = trabindiv;
        this.lenguaecolalico = lenguaecolalico;
        this.dificadapt = dificadapt;
        this.escolaborador = escolaborador;
        this.normassociales = normassociales;
        this.normasescolar = normasescolar;
        this.humor = humor;
        this.movestereot = movestereot;
        this.pataletas = pataletas;
        this.reaccionluces = reaccionluces;
        this.reaccionsonido = reaccionsonido;
        this.reaccionpersext = reaccionpersext;
        this.obsdesocial = obsdesocial;
        this.idfichaeduc = idfichaeduc;
    }

    public Long getIddesarrollosocial() {
        return iddesarrollosocial;
    }

    public void setIddesarrollosocial(Long iddesarrollosocial) {
        this.iddesarrollosocial = iddesarrollosocial;
    }

    public String getRelespontanea() {
        return relespontanea;
    }

    public void setRelespontanea(String relespontanea) {
        this.relespontanea = relespontanea;
    }

    public String getExplicomporta() {
        return explicomporta;
    }

    public void setExplicomporta(String explicomporta) {
        this.explicomporta = explicomporta;
    }

    public String getPartactgrup() {
        return partactgrup;
    }

    public void setPartactgrup(String partactgrup) {
        this.partactgrup = partactgrup;
    }

    public String getTrabindiv() {
        return trabindiv;
    }

    public void setTrabindiv(String trabindiv) {
        this.trabindiv = trabindiv;
    }

    public String getLenguaecolalico() {
        return lenguaecolalico;
    }

    public void setLenguaecolalico(String lenguaecolalico) {
        this.lenguaecolalico = lenguaecolalico;
    }

    public String getDificadapt() {
        return dificadapt;
    }

    public void setDificadapt(String dificadapt) {
        this.dificadapt = dificadapt;
    }

    public String getEscolaborador() {
        return escolaborador;
    }

    public void setEscolaborador(String escolaborador) {
        this.escolaborador = escolaborador;
    }

    public String getNormassociales() {
        return normassociales;
    }

    public void setNormassociales(String normassociales) {
        this.normassociales = normassociales;
    }

    public String getNormasescolar() {
        return normasescolar;
    }

    public void setNormasescolar(String normasescolar) {
        this.normasescolar = normasescolar;
    }

    public String getHumor() {
        return humor;
    }

    public void setHumor(String humor) {
        this.humor = humor;
    }

    public String getMovestereot() {
        return movestereot;
    }

    public void setMovestereot(String movestereot) {
        this.movestereot = movestereot;
    }

    public String getPataletas() {
        return pataletas;
    }

    public void setPataletas(String pataletas) {
        this.pataletas = pataletas;
    }

    public String getReaccionluces() {
        return reaccionluces;
    }

    public void setReaccionluces(String reaccionluces) {
        this.reaccionluces = reaccionluces;
    }

    public String getReaccionsonido() {
        return reaccionsonido;
    }

    public void setReaccionsonido(String reaccionsonido) {
        this.reaccionsonido = reaccionsonido;
    }

    public String getReaccionpersext() {
        return reaccionpersext;
    }

    public void setReaccionpersext(String reaccionpersext) {
        this.reaccionpersext = reaccionpersext;
    }

    public String getObsdesocial() {
        return obsdesocial;
    }

    public void setObsdesocial(String obsdesocial) {
        this.obsdesocial = obsdesocial;
    }

    public FichaEducacion getIdfichaeduc() {
        return idfichaeduc;
    }

    public void setIdfichaeduc(FichaEducacion idfichaeduc) {
        this.idfichaeduc = idfichaeduc;
    }
}
