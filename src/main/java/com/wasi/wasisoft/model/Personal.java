package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "personal")
@Access(AccessType.FIELD)

public class Personal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_personal")
    private Long id_personal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nombrepersonal")
    private String nombrepersonal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "apppaterno")
    private String apppaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "appmaterno")
    private String appmaterno;
    @Column(name = "telefono")
    private Integer telefono;

    @Column(name = "fcumple")
    @Temporal(TemporalType.DATE)
    private Date fcumple;

    @Column(name = "cargo")
    private String cargo;

    @Column(name = "correo")
    private String correo;

    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    @ManyToOne
    private Area idarea;

    public Personal() {
    }

    public Personal(Long id_personal) {
        this.id_personal = id_personal;
    }

    public Personal(String nombrepersonal, String apppaterno, String appmaterno, Integer telefono, Date fcumple, Area idarea) {
        this.nombrepersonal = nombrepersonal;
        this.apppaterno = apppaterno;
        this.appmaterno = appmaterno;
        this.telefono = telefono;
        this.fcumple = fcumple;
        this.idarea = idarea;
    }

    public Date getFcumple() {
        return fcumple;
    }

    public void setFcumple(Date fcumple) {
        this.fcumple = fcumple;
    }

    public Long getId_personal() {
        return id_personal;
    }

    public void setId_personal(Long idPersonal) {
        this.id_personal = idPersonal;
    }

    public String getNombrepersonal() {
        return nombrepersonal;
    }

    public void setNombrepersonal(String nombrepersonal) {
        this.nombrepersonal = nombrepersonal;
    }

    public String getApppaterno() {
        return apppaterno;
    }

    public void setApppaterno(String apppaterno) {
        this.apppaterno = apppaterno;
    }

    public String getAppmaterno() {
        return appmaterno;
    }

    public void setAppmaterno(String appmaterno) {
        this.appmaterno = appmaterno;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Area getIdarea() {
        return idarea;
    }

    public void setIdarea(Area idarea) {
        this.idarea = idarea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_personal != null ? id_personal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personal)) {
            return false;
        }
        Personal other = (Personal) object;
        if ((this.id_personal == null && other.id_personal != null) || (this.id_personal != null && !this.id_personal.equals(other.id_personal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Personal{" +
                "id_personal=" + id_personal +
                ", nombrepersonal='" + nombrepersonal + '\'' +
                ", apppaterno='" + apppaterno + '\'' +
                ", appmaterno='" + appmaterno + '\'' +
                ", telefono=" + telefono +
                ", id_area=" + idarea +
                '}';
    }
}
