package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "situacionecon")
@Access(AccessType.FIELD)
public class Situacionecon implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_situacionecon")
    private Long id_situacionecon;

    @Column(name = "numaportantes")
    private Integer numaportantes;

    @Column(name = "numdependientes")
    private Integer numdependientes;

    @Column(name = "madreing")
    private Double madreing;

    @Column(name = "padreing")
    private Double padreing;
    @Column(name = "hermanosing")
    private Double hermanosing;
    @Column(name = "abuelosing")
    private Double abuelosing;

    @Column(name = "primosing")
    private Double primosing;
    @Column(name = "parientesing")
    private Double parientesing;
    @Column(name = "tiosing")
    private Double tiosing;
    @Column(name = "noparientesing")
    private Double noparientesing;
    @Column(name = "totalingreso")
    private Double totalingreso;
    @Column(name = "alimentacionegr")
    private Double alimentacionegr;
    @Column(name = "viviendaegre")
    private Double viviendaegre;
    @Column(name = "educacionegre")
    private Double educacionegre;
    @Column(name = "aguaegre")
    private Double aguaegre;
    @Column(name = "luzegre")
    private Double luzegre;
    @Column(name = "telefonoegre")
    private Double telefonoegre;
    @Column(name = "pasajesegre")
    private Double pasajesegre;
    @Column(name = "otrosegre")
    private Double otrosegre;
    @Column(name = "totalegre")
    private Double totalegre;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public Situacionecon() {
    }

    public Situacionecon(Integer numdependientes, Double madreing, Double padreing,Double hermanosing, Double abuelosing, Double primosing, Double parientesing, Double tiosing, Double noparientesing, Double totalingreso, Double alimentacionegr, Double viviendaegre, Double educacionegre, Double aguaegre, Double luzegre, Double telefonoegre, Double pasajesegre,Double otrosegre, Double totalegre,Expediente idexpediente ) {
        this.numdependientes = numdependientes;
        this.madreing = madreing;
        this.padreing = padreing;
        this.hermanosing = hermanosing;
        this.abuelosing = abuelosing;
        this.primosing = primosing;
        this.parientesing = parientesing;
        this.tiosing = tiosing;
        this.noparientesing = noparientesing;
        this.totalingreso = totalingreso;
        this.alimentacionegr = alimentacionegr;
        this.viviendaegre = viviendaegre;
        this.educacionegre = educacionegre;
        this.aguaegre = aguaegre;
        this.luzegre = luzegre;
        this.telefonoegre = telefonoegre;
        this.pasajesegre = pasajesegre;
        this.otrosegre = otrosegre;
        this.totalegre = totalegre;
        this.idexpediente = idexpediente;
    }

    public Double getHermanosing() {
        return hermanosing;
    }

    public void setHermanosing(Double hermanosing) {
        this.hermanosing = hermanosing;
    }

    public Long getId_situacionecon() {
        return id_situacionecon;
    }

    public void setId_situacionecon(Long id_situacionecon) {
        this.id_situacionecon = id_situacionecon;
    }

    public Integer getNumaportantes() {
        return numaportantes;
    }

    public void setNumaportantes(Integer numaportantes) {
        this.numaportantes = numaportantes;
    }

    public Integer getNumdependientes() {
        return numdependientes;
    }

    public void setNumdependientes(Integer numdependientes) {
        this.numdependientes = numdependientes;
    }

    public Double getMadreing() {
        return madreing;
    }

    public void setMadreing(Double madreing) {
        this.madreing = madreing;
    }

    public Double getPadreing() {
        return padreing;
    }

    public void setPadreing(Double padreing) {
        this.padreing = padreing;
    }

    public Double getAbuelosing() {
        return abuelosing;
    }

    public void setAbuelosing(Double abuelosing) {
        this.abuelosing = abuelosing;
    }

    public Double getPrimosing() {
        return primosing;
    }

    public void setPrimosing(Double primosing) {
        this.primosing = primosing;
    }

    public Double getParientesing() {
        return parientesing;
    }

    public void setParientesing(Double parientesing) {
        this.parientesing = parientesing;
    }

    public Double getTiosing() {
        return tiosing;
    }

    public void setTiosing(Double tiosing) {
        this.tiosing = tiosing;
    }

    public Double getNoparientesing() {
        return noparientesing;
    }

    public void setNoparientesing(Double noparientesing) {
        this.noparientesing = noparientesing;
    }

    public Double getTotalingreso() {
        return totalingreso;
    }

    public void setTotalingreso(Double totalingreso) {
        this.totalingreso = totalingreso;
    }

    public Double getAlimentacionegr() {
        return alimentacionegr;
    }

    public void setAlimentacionegr(Double alimentacionegr) {
        this.alimentacionegr = alimentacionegr;
    }

    public Double getViviendaegre() {
        return viviendaegre;
    }

    public void setViviendaegre(Double viviendaegre) {
        this.viviendaegre = viviendaegre;
    }

    public Double getEducacionegre() {
        return educacionegre;
    }

    public void setEducacionegre(Double educacionegre) {
        this.educacionegre = educacionegre;
    }

    public Double getAguaegre() {
        return aguaegre;
    }

    public void setAguaegre(Double aguaegre) {
        this.aguaegre = aguaegre;
    }

    public Double getLuzegre() {
        return luzegre;
    }

    public void setLuzegre(Double luzegre) {
        this.luzegre = luzegre;
    }

    public Double getTelefonoegre() {
        return telefonoegre;
    }

    public void setTelefonoegre(Double telefonoegre) {
        this.telefonoegre = telefonoegre;
    }

    public Double getPasajesegre() {
        return pasajesegre;
    }

    public void setPasajesegre(Double pasajesegre) {
        this.pasajesegre = pasajesegre;
    }

    public Double getOtrosegre() {
        return otrosegre;
    }

    public void setOtrosegre(Double otrosegre) {
        this.otrosegre = otrosegre;
    }

    public Double getTotalegre() {
        return totalegre;
    }

    public void setTotalegre(Double totalegre) {
        this.totalegre = totalegre;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
