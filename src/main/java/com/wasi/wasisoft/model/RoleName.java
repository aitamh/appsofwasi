package com.wasi.wasisoft.model;

/**
 * Created by HeverFernandez on 07/12/17.
 */
public enum  RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_ATO,
    ROLE_CENTRO,
    ROLE_EDUCA,
    ROLE_FISIO,
    ROLE_GINGR,
    ROLE_PSICO,
    ROLE_SALUD,
    ROLE_SOCIAL,
    ROLE_VATO,
    ROLE_VCDIA,
    ROLE_VCOMU,
    ROLE_VCRA,
    ROLE_VPSICO
}
