package com.wasi.wasisoft.model;


import javax.persistence.*;


@Entity
@Table(name = "departamento")
@Access(AccessType.FIELD)
public class Departamento {

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "id_depa")
        private Long id_depa;

        @Column(name = "departamento")
        private String nombre_departamento;

    public Long getId_depa() {
        return id_depa;
    }

    public void setId_depa(Long id_depa) {
        this.id_depa = id_depa;
    }

    public String getNombre_departamento() {
        return nombre_departamento;
    }

    public void setNombre_departamento(String nombre_departamento) {
        this.nombre_departamento = nombre_departamento;
    }
}
