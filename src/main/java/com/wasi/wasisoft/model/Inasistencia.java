package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "inasistencia")
public class Inasistencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_inasistencia")
    private Long idinasistencia;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "accion_realizadada")
    private String accionrealizada;

    @Column(name = "finasistencia")
    private Date finasistencia;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private FichaTerapiaFisica idfichaterapiafisica;

    public Inasistencia(String descripcion, String accionrealizada, Date finasistencia, FichaTerapiaFisica idfichaterapiafisica) {
        this.descripcion = descripcion;
        this.accionrealizada = accionrealizada;
        this.finasistencia = finasistencia;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Inasistencia() {}

    public Long getIdinasistencia() {
        return idinasistencia;
    }

    public void setIdinasistencia(Long idinasistencia) {
        this.idinasistencia = idinasistencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAccionrealizada() {
        return accionrealizada;
    }

    public void setAccionrealizada(String accionrealizada) {
        this.accionrealizada = accionrealizada;
    }

    public Date getFinasistencia() {
        return finasistencia;
    }

    public void setFinasistencia(Date finasistencia) {
        this.finasistencia = finasistencia;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Inasistencia)) return false;

        Inasistencia that = (Inasistencia) o;

        if (getIdinasistencia() != null ? !getIdinasistencia().equals(that.getIdinasistencia()) : that.getIdinasistencia() != null)
            return false;
        if (getDescripcion() != null ? !getDescripcion().equals(that.getDescripcion()) : that.getDescripcion() != null)
            return false;
        if (getAccionrealizada() != null ? !getAccionrealizada().equals(that.getAccionrealizada()) : that.getAccionrealizada() != null)
            return false;
        if (getFinasistencia() != null ? !getFinasistencia().equals(that.getFinasistencia()) : that.getFinasistencia() != null)
            return false;
        return getIdfichaterapiafisica() != null ? getIdfichaterapiafisica().equals(that.getIdfichaterapiafisica()) : that.getIdfichaterapiafisica() == null;
    }


}
