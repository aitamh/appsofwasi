package com.wasi.wasisoft.model;

import javax.persistence.*;

@Entity
@Table(name = "situacionpaciente")
public class Situacionpaciente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_situacionpaciente")
    private Long idsituacionpaciente;

    @Column(name = "pacienteaceptado")
    private Boolean pacienteaceptado;

    @Column(name = "estado")
    private Boolean estado;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @OneToOne
    private Expediente idexpediente;

    public Situacionpaciente() {
    }

    public Situacionpaciente(Boolean pacienteaceptado, Boolean estado, Expediente idexpediente) {
        this.pacienteaceptado = pacienteaceptado;
        this.estado = estado;
        this.idexpediente = idexpediente;
    }

    public Long getIdsituacionpaciente() {
        return idsituacionpaciente;
    }

    public void setIdsituacionpaciente(Long idsituacionpaciente) {
        this.idsituacionpaciente = idsituacionpaciente;
    }

    public Boolean getPacienteaceptado() {
        return pacienteaceptado;
    }

    public void setPacienteaceptado(Boolean pacienteaceptado) {
        this.pacienteaceptado = pacienteaceptado;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
