package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@Entity
@Table (name = "visdomintegral")
public class Visdomintegral implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_visdomintegral")
    private Long idvisdomintegral;

    @Column(name = "numfichaatenc")
    private Integer numfichaatenc;
    //Indica el numero de seguimineto (1, 2 y 3)
    @Column(name = "numseguimiento")
    private Integer numseguimiento;

    @Column(name = "edad")
    private Integer edad;

    @Column(name = "fechavisita")
    @Temporal(TemporalType.DATE)
    private Date fechavisita;

    @Size(max = 20)
    @Column(name = "criteriouno")
    private Integer criteriouno;
    @Size(max = 20)
    @Column(name = "criteriodos")
    private Integer criteriodos;
    @Size(max = 20)
    @Column(name = "criteriotres")
    private Integer criteriotres;
    @Size(max = 20)
    @Column(name = "criteriocuatro")
    private Integer criteriocuatro;
    @Size(max = 20)
    @Column(name = "puntajetotal")
    private Integer puntajetotal;

    @JoinColumn(name = "id_fichasalud", referencedColumnName = "id_fichasalud")
    @ManyToOne
    private FichaSalud idfichasalud;

    public Visdomintegral() {
    }

    public Visdomintegral( Long idvisdomintegral) {
        this.idvisdomintegral = idvisdomintegral;
    }

    public Visdomintegral(Integer numfichaatenc, Integer numseguimiento, Integer edad, Date fechavisita, Integer criteriouno, Integer criteriodos, Integer criteriotres, Integer criteriocuatro, Integer puntajetotal, FichaSalud idfichasalud) {
        this.numfichaatenc = numfichaatenc;
        this.numseguimiento = numseguimiento;
        this.edad = edad;
        this.fechavisita = fechavisita;
        this.criteriouno = criteriouno;
        this.criteriodos = criteriodos;
        this.criteriotres = criteriotres;
        this.criteriocuatro = criteriocuatro;
        this.puntajetotal = puntajetotal;
        this.idfichasalud = idfichasalud;
    }

    public Long getIdvisdomintegral() {
        return idvisdomintegral;
    }

    public void setIdvisdomintegral(Long idvisdomintegral) {
        this.idvisdomintegral = idvisdomintegral;
    }

    public Integer getNumfichaatenc() {
        return numfichaatenc;
    }

    public void setNumfichaatenc(Integer numfichaatenc) {
        this.numfichaatenc = numfichaatenc;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getNumseguimiento() {
        return numseguimiento;
    }

    public void setNumseguimiento(Integer numseguimiento) {
        this.numseguimiento = numseguimiento;
    }

    public Date getFechavisita() {
        return fechavisita;
    }

    public void setFechavisita(Date fechavisita) {
        this.fechavisita = fechavisita;
    }

    public Integer getCriteriouno() {
        return criteriouno;
    }

    public void setCriteriouno(Integer criteriouno) {
        this.criteriouno = criteriouno;
    }

    public Integer getCriteriodos() {
        return criteriodos;
    }

    public void setCriteriodos(Integer criteriodos) {
        this.criteriodos = criteriodos;
    }

    public Integer getCriteriotres() {
        return criteriotres;
    }

    public void setCriteriotres(Integer criteriotres) {
        this.criteriotres = criteriotres;
    }

    public Integer getCriteriocuatro() {
        return criteriocuatro;
    }

    public void setCriteriocuatro(Integer criteriocuatro) {
        this.criteriocuatro = criteriocuatro;
    }

    public Integer getPuntajetotal() {
        return puntajetotal;
    }

    public void setPuntajetotal(Integer puntajetotal) {
        this.puntajetotal = puntajetotal;
    }

    public FichaSalud getIdfichasalud() {
        return idfichasalud;
    }

    public void setIdfichasalud(FichaSalud idfichasalud) {
        this.idfichasalud = idfichasalud;
    }
}
