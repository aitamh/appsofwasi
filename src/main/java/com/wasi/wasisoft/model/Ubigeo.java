package com.wasi.wasisoft.model;

import javax.persistence.*;

@Entity
@Table(name = "ubigeo")
@Access(AccessType.FIELD)
public class Ubigeo {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "id_ubigeo")
        private Long id_ubigeo;

        @JoinColumn(name = "id_depa", referencedColumnName = "id_depa")
        @ManyToOne
        private Departamento id_depa;

        @JoinColumn(name = "id_prov", referencedColumnName = "id_prov")
        @ManyToOne
        private Provincia id_prov;

        @JoinColumn(name = "id_dist", referencedColumnName = "id_dist")
        @ManyToOne
        private Distrito id_dist;

    public Long getId_ubigeo() {
        return id_ubigeo;
    }

    public void setId_ubigeo(Long id_ubigeo) {
        this.id_ubigeo = id_ubigeo;
    }

    public Departamento getId_depa() {
        return id_depa;
    }

    public void setId_depa(Departamento id_depa) {
        this.id_depa = id_depa;
    }

    public Provincia getId_prov() {
        return id_prov;
    }

    public void setId_prov(Provincia id_prov) {
        this.id_prov = id_prov;
    }

    public Distrito getId_dist() {
        return id_dist;
    }

    public void setId_dist(Distrito id_dist) {
        this.id_dist = id_dist;
    }
}
