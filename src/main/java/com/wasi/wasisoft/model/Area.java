package com.wasi.wasisoft.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "area")

public class Area implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area")
    private Long id_area;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcionarea")
    private String descripcionarea;

    public Area() {
    }

    public Area(Long id_area) {
        this.id_area = id_area;
    }

    public Area(String descripcionarea) {
        this.descripcionarea = descripcionarea;
    }

    public Long getId_area() {
        return id_area;
    }

    public void setId_area(Long id_area) {
        this.id_area = id_area;
    }

    public String getDescripcionarea() {
        return descripcionarea;
    }

    public void setDescripcionarea(String descripcionarea) {
        this.descripcionarea = descripcionarea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_area != null ? id_area.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.id_area == null && other.id_area != null) || (this.id_area != null && !this.id_area.equals(other.id_area))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wasi.wasisoft.model.Area[ idArea=" + id_area + " ]";
    }

}

