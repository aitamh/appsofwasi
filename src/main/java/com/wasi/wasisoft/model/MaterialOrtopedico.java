package com.wasi.wasisoft.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wasi.wasisoft.json.JsonDateSimpleDeserializer;
import com.wasi.wasisoft.json.JsonDateSimpleSerializer;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "materialortopedico")
@Access(AccessType.FIELD)
public class MaterialOrtopedico {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_materialortopedico", unique = true, nullable = false)
    private Long idmaterialortopedico;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "cantidad", nullable = false)
    private Integer cantidad;

    @Column(name = "fregistro")
    @Temporal(TemporalType.DATE)
    private Date fregistro;

    @Column(name = "modalidad")
    private String modalidad;

    @Column(name = "costo")
    private Double costo;

    public MaterialOrtopedico() {
    }

    public MaterialOrtopedico(Long idmaterialortopedico) {
    this.idmaterialortopedico = idmaterialortopedico;
    }

    public MaterialOrtopedico(String nombre, Integer cantidad, Date fregistro, String modalidad, Double costo) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.fregistro = fregistro;
        this.modalidad = modalidad;
        this.costo = costo;
    }

    public Long getIdmaterialortopedico() {
        return idmaterialortopedico;
    }

    public void setIdmaterialortopedico(Long idmaterialortopedico) {
        this.idmaterialortopedico = idmaterialortopedico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }
}
