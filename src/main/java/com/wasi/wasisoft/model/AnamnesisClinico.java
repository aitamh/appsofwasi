package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table (name = "anamclinico")
public class AnamnesisClinico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_anamclin")
    private Long idanamnesis;

    @Column(name = "edad_embarazo")
    private Integer edad_embarazo;

    @Column(name = "numero_hijos")
    private Integer numero_hijos;

    @Column(name = "num_embarazoaborto")
    private String num_emb_abortos;

    @Column(name = "control_medico")
    private String control_medico;
    @Column(name = "compli_embarazo")
    private String compli_embarazo;
    @Column(name = "infeccion")
    private String infeccion;
    @Column(name = "perdidosangre")
    private String perdidosangre;
    @Column(name = "trauma")
    private String trauma;
    @Column (name = "crecimientofeto")
    private String crecimientofeto;
    @Column (name = "otrocompli")
    private String otrocompli;
    @Column(name = "usa_medicamentos")
    private String usa_medicamento;
    @Column(name = "otrainformacion")
    private String otrainformacion;
    @Column(name = "lugar_parto")
    private String lugar_parto;

    @Column(name = "num_meses_parto")
    private Integer num_meses_parto;

    @Column(name = "personal_parto")
    private String personal_parto;

    @Column(name = "compli_parto")
    private String compli_parto;

    @Column(name = "progreso_parto")
    private String progreso_parto;

    @Column(name = "compli_postparto")
    private String compli_postparto;

    @Column(name = "talla")
    private Double talla;

    @Column(name = "peso")
    private Double peso;

    @Column(name = "apgar")
    private String apgar;

    @Column(name = "color")
    private String color;

    @Column(name = "llorar")
    private String llora;

    @Column(name = "movimiento")
    private String movimiento;

    @Column(name = "respiracion")
    private String respiracion;

    @Column(name = "latido_corazon")
    private String latido_corazon;

    @Column(name = "progreso")
    private String progreso;

    @Column(name = "eval_medicas")
    private String eval_medicas;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @OneToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public AnamnesisClinico() {
    }

    public AnamnesisClinico(Long idanamnesis) {
        this.idanamnesis = idanamnesis;
    }


    public AnamnesisClinico(Integer edad_embarazo, Integer numero_hijos, String num_emb_abortos, String control_medico, String compli_embarazo, String infeccion, String perdidosangre, String trauma, String crecimientofeto, String otrocompli, String usa_medicamento, String otrainformacion, String lugar_parto, Integer num_meses_parto, String personal_parto, String compli_parto, String progreso_parto, String compli_postparto, Double talla, Double peso, String apgar, String color, String llora, String movimiento, String respiracion, String latido_corazon, String progreso, String eval_medicas, FichaTerapiaFisica idfichaterapiafisica) {
        this.edad_embarazo = edad_embarazo;
        this.numero_hijos = numero_hijos;
        this.num_emb_abortos = num_emb_abortos;
        this.control_medico = control_medico;
        this.compli_embarazo = compli_embarazo;
        this.infeccion = infeccion;
        this.perdidosangre = perdidosangre;
        this.trauma = trauma;
        this.crecimientofeto = crecimientofeto;
        this.otrocompli = otrocompli;
        this.usa_medicamento = usa_medicamento;
        this.otrainformacion = otrainformacion;
        this.lugar_parto = lugar_parto;
        this.num_meses_parto = num_meses_parto;
        this.personal_parto = personal_parto;
        this.compli_parto = compli_parto;
        this.progreso_parto = progreso_parto;
        this.compli_postparto = compli_postparto;
        this.talla = talla;
        this.peso = peso;
        this.apgar = apgar;
        this.color = color;
        this.llora = llora;
        this.movimiento = movimiento;
        this.respiracion = respiracion;
        this.latido_corazon = latido_corazon;
        this.progreso = progreso;
        this.eval_medicas = eval_medicas;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Long getIdanamnesis() {
        return idanamnesis;
    }

    public void setIdanamnesis(Long idanamnesis) {
        this.idanamnesis = idanamnesis;
    }

    public Integer getEdad_embarazo() {
        return edad_embarazo;
    }

    public void setEdad_embarazo(Integer edad_embarazo) {
        this.edad_embarazo = edad_embarazo;
    }

    public Integer getNumero_hijos() {
        return numero_hijos;
    }

    public void setNumero_hijos(Integer numero_hijos) {
        this.numero_hijos = numero_hijos;
    }

    public String getControl_medico() {
        return control_medico;
    }

    public void setControl_medico(String control_medico) {
        this.control_medico = control_medico;
    }

    public String getUsa_medicamento() {
        return usa_medicamento;
    }

    public void setUsa_medicamento(String usa_medicamento) {
        this.usa_medicamento = usa_medicamento;
    }

    public String getLugar_parto() {
        return lugar_parto;
    }

    public void setLugar_parto(String lugar_parto) {
        this.lugar_parto = lugar_parto;
    }

    public Integer getNum_meses_parto() {
        return num_meses_parto;
    }

    public void setNum_meses_parto(Integer num_meses_parto) {
        this.num_meses_parto = num_meses_parto;
    }

    public String getPersonal_parto() {
        return personal_parto;
    }

    public void setPersonal_parto(String personal_parto) {
        this.personal_parto = personal_parto;
    }

    public String getCompli_parto() {
        return compli_parto;
    }

    public void setCompli_parto(String compli_parto) {
        this.compli_parto = compli_parto;
    }

    public String getProgreso_parto() {
        return progreso_parto;
    }

    public void setProgreso_parto(String progreso_parto) {
        this.progreso_parto = progreso_parto;
    }

    public String getCompli_postparto() {
        return compli_postparto;
    }

    public void setCompli_postparto(String compli_postparto) {
        this.compli_postparto = compli_postparto;
    }

    public Double getTalla() {
        return talla;
    }

    public void setTalla(Double talla) {
        this.talla = talla;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getApgar() {
        return apgar;
    }

    public void setApgar(String apgar) {
        this.apgar = apgar;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLlora() {
        return llora;
    }

    public void setLlora(String llora) {
        this.llora = llora;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getRespiracion() {
        return respiracion;
    }

    public void setRespiracion(String respiracion) {
        this.respiracion = respiracion;
    }

    public String getLatido_corazon() {
        return latido_corazon;
    }

    public void setLatido_corazon(String latido_corazon) {
        this.latido_corazon = latido_corazon;
    }

    public String getProgreso() {
        return progreso;
    }

    public void setProgreso(String progreso) {
        this.progreso = progreso;
    }

    public String getEval_medicas() {
        return eval_medicas;
    }

    public void setEval_medicas(String eval_medicas) {
        this.eval_medicas = eval_medicas;
    }

    public String getNum_emb_abortos() {
        return num_emb_abortos;
    }

    public void setNum_emb_abortos(String num_emb_abortos) {
        this.num_emb_abortos = num_emb_abortos;
    }

    public String getInfeccion() {
        return infeccion;
    }

    public void setInfeccion(String infeccion) {
        this.infeccion = infeccion;
    }

    public String getPerdidosangre() {
        return perdidosangre;
    }

    public void setPerdidosangre(String perdidosangre) {
        this.perdidosangre = perdidosangre;
    }

    public String getTrauma() {
        return trauma;
    }

    public void setTrauma(String trauma) {
        this.trauma = trauma;
    }

    public String getCrecimientofeto() {
        return crecimientofeto;
    }

    public void setCrecimientofeto(String crecimientofeto) {
        this.crecimientofeto = crecimientofeto;
    }

    public String getOtrocompli() {
        return otrocompli;
    }

    public void setOtrocompli(String otrocompli) {
        this.otrocompli = otrocompli;
    }

    public String getOtrainformacion() {
        return otrainformacion;
    }

    public void setOtrainformacion(String otrainformacion) {
        this.otrainformacion = otrainformacion;
    }

    public String getCompli_embarazo() {
        return compli_embarazo;
    }

    public void setCompli_embarazo(String compli_embarazo) {
        this.compli_embarazo = compli_embarazo;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }
}
