package com.wasi.wasisoft.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "preinscripcion")
@Access(AccessType.FIELD)
public class Preinscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_preinscripcion")
    private Long id_preinscripcion;

    @Column(name = "nombrepaciente")
    private String nombre_paciente;

    @Column (name = "condicionpaciente")
    private String condicionpaciente;

    @Column (name = "edadpaciente")
    private Integer edadpaciente;

    @Column (name = "fechanacimiento")
    @Temporal(TemporalType.DATE)
//    @JsonFormat(pattern = "dd::MM::yyyy")
    private Date fechanacimiento;

    @Column (name = "diagnostico")
    private String diagnosticopaciente;

    @Column (name = "fechainscripcion")
    @Temporal(TemporalType.DATE)
    private Date fechainscripcionpaciente;

    @Column (name = "nombrepadre")
    private String nombrepadre;

    @Column (name = "nombremadre")
    private String nombremadre;

    @Column (name = "ocupacion")
    private String ocupacion;

    @Column (name = "celular1")
    private Integer celular1;

    @Column (name = "celular2")
    private Integer celular2;

    @Column(name = "llamada1")
    @Temporal(TemporalType.DATE)
    private Date llamada1;

    @Column(name = "llamada2")
    @Temporal(TemporalType.DATE)
    private Date llamada2;

    @Column(name = "llamada3")
    @Temporal(TemporalType.DATE)
    private Date llamada3;

    @Size(max = 100)
    @Column(name = "observacion")
    private String observacion;

    @Column(name = "estado")
    private Boolean estado;

    public Preinscripcion() {
    }

    public Preinscripcion(String nombre_paciente, String condicionpaciente, Integer edadpaciente, Date fechanacimiento, String diagnosticopaciente, Date fechainscripcionpaciente, String nombrepadre, String nombremadre, String ocupacion, Integer celular1, Integer celular2, Date llamada1, Date llamada2, Date llamada3, @Size(max = 100) String observacion, Boolean estado) {
        this.nombre_paciente = nombre_paciente;
        this.condicionpaciente = condicionpaciente;
        this.edadpaciente = edadpaciente;
        this.fechanacimiento = fechanacimiento;
        this.diagnosticopaciente = diagnosticopaciente;
        this.fechainscripcionpaciente = fechainscripcionpaciente;
        this.nombrepadre = nombrepadre;
        this.nombremadre = nombremadre;
        this.ocupacion = ocupacion;
        this.celular1 = celular1;
        this.celular2 = celular2;
        this.llamada1 = llamada1;
        this.llamada2 = llamada2;
        this.llamada3 = llamada3;
        this.observacion = observacion;
        this.estado = estado;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_preinscripcion() {
        return id_preinscripcion;
    }

    public void setId_preinscripcion(Long id_preinscripcion) {
        this.id_preinscripcion = id_preinscripcion;
    }

    public String getNombre_paciente() {
        return nombre_paciente;
    }

    public void setNombre_paciente(String nombre_paciente) {
        this.nombre_paciente = nombre_paciente;
    }

    public String getCondicionpaciente() {
        return condicionpaciente;
    }

    public void setCondicionpaciente(String condicionpaciente) {
        this.condicionpaciente = condicionpaciente;
    }

    public Integer getEdadpaciente() {
        return edadpaciente;
    }

    public void setEdadpaciente(Integer edadpaciente) {
        this.edadpaciente = edadpaciente;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getDiagnosticopaciente() {
        return diagnosticopaciente;
    }

    public void setDiagnosticopaciente(String diagnosticopaciente) {
        this.diagnosticopaciente = diagnosticopaciente;
    }

    public Date getFechainscripcionpaciente() {
        return fechainscripcionpaciente;
    }

    public void setFechainscripcionpaciente(Date fechainscripcionpaciente) {
        this.fechainscripcionpaciente = fechainscripcionpaciente;
    }

    public String getNombrepadre() {
        return nombrepadre;
    }

    public void setNombrepadre(String nombrepadre) {
        this.nombrepadre = nombrepadre;
    }

    public String getNombremadre() {
        return nombremadre;
    }

    public void setNombremadre(String nombremadre) {
        this.nombremadre = nombremadre;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public Integer getCelular1() {
        return celular1;
    }

    public void setCelular1(Integer celular1) {
        this.celular1 = celular1;
    }

    public Integer getCelular2() {
        return celular2;
    }

    public void setCelular2(Integer celular2) {
        this.celular2 = celular2;
    }

    public Date getLlamada1() {
        return llamada1;
    }

    public void setLlamada1(Date llamada1) {
        this.llamada1 = llamada1;
    }

    public Date getLlamada2() {
        return llamada2;
    }

    public void setLlamada2(Date llamada2) {
        this.llamada2 = llamada2;
    }

    public Date getLlamada3() {
        return llamada3;
    }

    public void setLlamada3(Date llamada3) {
        this.llamada3 = llamada3;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}

