package com.wasi.wasisoft.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "medicamento")
@Access(AccessType.FIELD)
public class Medicamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_medicamento")
    private Long idMedicamento;

    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombremedic")
    private String nombremedic;
    @Size(max = 100)
    @Column(name = "caracteristica")
    private String caracteristica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "stock")
    private int stock;
    @Column(name = "comprado")
    private String comprado;

    @Column(name = "fecha_caducidad")
    @Temporal(TemporalType.DATE)
//    @JsonFormat(pattern = "dd::MM::yyyy")
    private Date fecha_caducidad;

    @Size(max = 20)
    @Column(name = "costoventa")
    private String costoventa;
    @Column(name = "salida")
    private String salida;
    @Size(max = 50)
    @Column(name = "tipo_medicamento")
    private String tipo_medicamento;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    public String getComprado() {
        return comprado;
    }

    public void setComprado(String comprado) {
        this.comprado = comprado;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(Long idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getNombremedic() {
        return nombremedic;
    }

    public void setNombremedic(String nombremedic) {
        this.nombremedic = nombremedic;
    }

    public String getCostoventa() {
        return costoventa;
    }

    public void setCostoventa(String costoventa) {
        this.costoventa = costoventa;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Date getFecha_caducidad() {
        return fecha_caducidad;
    }

    public void setFecha_caducidad(Date fecha_caducidad) {
        this.fecha_caducidad = fecha_caducidad;
    }

    public String getTipo_medicamento() {
        return tipo_medicamento;
    }

    public void setTipo_medicamento(String tipo_medicamento) {
        this.tipo_medicamento = tipo_medicamento;
    }

    @Override
    public String toString() {
        return "Medicamento{" +
                "idMedicamento=" + idMedicamento +
                ", codigo='" + codigo + '\'' +
                ", nombremedic='" + nombremedic + '\'' +
                ", caracteristica='" + caracteristica + '\'' +
                ", stock=" + stock +
                ", comprado='" + comprado + '\'' +
                ", fecha_caducidad=" + fecha_caducidad +
                ", costoventa='" + costoventa + '\'' +
                ", salida='" + salida + '\'' +
                ", tipo_medicamento='" + tipo_medicamento + '\'' +
                '}';
    }
}
