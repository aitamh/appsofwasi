package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@Entity
@Table(name = "distribmedic")
public class MedicamentoVenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_distribmedic")
    private Long idmedicventa;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "motivoadmision")
    private String motivoadmision;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dosis")
    private String dosis;

    @JoinColumn(name = "id_medicamentos", referencedColumnName = "id_medicamento")
    @ManyToOne
    private Medicamento idmedicamentos;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public MedicamentoVenta() {
    }
    public MedicamentoVenta(Long idmedicventa) {
        this.idmedicventa = idmedicventa;
    }

    public MedicamentoVenta(Date fecha, String motivoadmision, String dosis, Medicamento idmedicamentos, Expediente idexpediente) {
        this.fecha = fecha;
        this.motivoadmision = motivoadmision;
        this.dosis = dosis;
        this.idmedicamentos = idmedicamentos;
        this.idexpediente = idexpediente;
    }

    public Long getIdmedicventa() {
        return idmedicventa;
    }

    public void setIdmedicventa(Long idmedicventa) {
        this.idmedicventa = idmedicventa;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getMotivoadmision() {
        return motivoadmision;
    }

    public void setMotivoadmision(String motivoadmision) {
        this.motivoadmision = motivoadmision;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public Medicamento getIdmedicamentos() {
        return idmedicamentos;
    }

    public void setIdmedicamentos(Medicamento idmedicamentos) {
        this.idmedicamentos = idmedicamentos;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
