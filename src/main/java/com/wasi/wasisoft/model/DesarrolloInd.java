package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "desainde")
public class DesarrolloInd implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_desainde")
    private Long iddesainde;

    @Column(name = "des_boca_abajo")
    private String des_boca_abajo;

    @Column(name = "des_levantar_cabeza")
    private String des_levantar_cabeza;

    @Column(name = "des_voltearse")
    private String des_voltearse;

    @Column(name = "des_sentarse")
    private String des_sentarse;

    @Column(name = "des_gatear")
    private String des_gatear;

    @Column(name = "des_caminar")
    private String des_caminar;

    @Column(name = "ind_transferencias")
    private String ind_transferencias;

    @Column(name = "ind_lavarse")
    private String ind_lavarse;

    @Column(name = "ind_limpio_bano")
    private String ind_limpio_bano;

    @Column(name = "ind_lav_diente")
    private String ind_lav_diente;

    @Column(name = "ind_peinarse")
    private String ind_peinarse;

    @Column(name = "ind_vestirse")
    private String ind_vestirse;

    @Column(name = "ind_comer")
    private String ind_comer;

    @Column(name = "ind_tomar")
    private String ind_tomar;

    @Column(name = "ind_hablar")
    private String ind_hablar;

    @Column(name = "ind_ver")
    private String ind_ver;

    @Column(name = "ind_ayudar_casa")
    private String ind_ayudar_casa;

    @Column(name = "ind_escuela")
    private String ind_escuela;

    @Column(name = "ind_comunicacion")
    private String ind_comunicacion;

    @Column(name = "ind_amigos")
    private String ind_amigos;

    @Column(name = "ind_pasa_tiempos")
    private String ind_pasa_tiempos;

    @Column(name = "ind_oir")
    private String ind_oir;

    @JoinColumn(name = "id_anamclin", referencedColumnName = "id_anamclin")
    @OneToOne
    private AnamnesisClinico idanamnesis;

    public DesarrolloInd() {
    }

    public DesarrolloInd(Long iddesainde){
        this.iddesainde = iddesainde;
    }

    public DesarrolloInd(String des_boca_abajo, String des_levantar_cabeza, String des_voltearse, String des_sentarse, String des_gatear, String des_caminar, String ind_transferencias, String ind_lavarse, String ind_limpio_bano, String ind_lav_diente, String ind_peinarse, String ind_vestirse, String ind_comer, String ind_tomar, String ind_hablar, String ind_ver, String ind_ayudar_casa, String ind_escuela, String ind_comunicacion, String ind_amigos, String ind_pasa_tiempos, String ind_oir, AnamnesisClinico idanamnesis) {
        this.des_boca_abajo = des_boca_abajo;
        this.des_levantar_cabeza = des_levantar_cabeza;
        this.des_voltearse = des_voltearse;
        this.des_sentarse = des_sentarse;
        this.des_gatear = des_gatear;
        this.des_caminar = des_caminar;
        this.ind_transferencias = ind_transferencias;
        this.ind_lavarse = ind_lavarse;
        this.ind_limpio_bano = ind_limpio_bano;
        this.ind_lav_diente = ind_lav_diente;
        this.ind_peinarse = ind_peinarse;
        this.ind_vestirse = ind_vestirse;
        this.ind_comer = ind_comer;
        this.ind_tomar = ind_tomar;
        this.ind_hablar = ind_hablar;
        this.ind_ver = ind_ver;
        this.ind_ayudar_casa = ind_ayudar_casa;
        this.ind_escuela = ind_escuela;
        this.ind_comunicacion = ind_comunicacion;
        this.ind_amigos = ind_amigos;
        this.ind_pasa_tiempos = ind_pasa_tiempos;
        this.ind_oir = ind_oir;
        this.idanamnesis = idanamnesis;
    }

    public Long getIddesainde() {
        return iddesainde;
    }

    public void setIddesainde(Long iddesainde) {
        this.iddesainde = iddesainde;
    }

    public String getDes_boca_abajo() {
        return des_boca_abajo;
    }

    public void setDes_boca_abajo(String des_boca_abajo) {
        this.des_boca_abajo = des_boca_abajo;
    }

    public String getDes_levantar_cabeza() {
        return des_levantar_cabeza;
    }

    public void setDes_levantar_cabeza(String des_levantar_cabeza) {
        this.des_levantar_cabeza = des_levantar_cabeza;
    }

    public String getDes_voltearse() {
        return des_voltearse;
    }

    public void setDes_voltearse(String des_voltearse) {
        this.des_voltearse = des_voltearse;
    }

    public String getDes_sentarse() {
        return des_sentarse;
    }

    public void setDes_sentarse(String des_sentarse) {
        this.des_sentarse = des_sentarse;
    }

    public String getDes_gatear() {
        return des_gatear;
    }

    public void setDes_gatear(String des_gatear) {
        this.des_gatear = des_gatear;
    }

    public String getDes_caminar() {
        return des_caminar;
    }

    public void setDes_caminar(String des_caminar) {
        this.des_caminar = des_caminar;
    }

    public String getInd_transferencias() {
        return ind_transferencias;
    }

    public void setInd_transferencias(String ind_transferencias) {
        this.ind_transferencias = ind_transferencias;
    }

    public String getInd_lavarse() {
        return ind_lavarse;
    }

    public void setInd_lavarse(String ind_lavarse) {
        this.ind_lavarse = ind_lavarse;
    }

    public String getInd_limpio_bano() {
        return ind_limpio_bano;
    }

    public void setInd_limpio_bano(String ind_limpio_bano) {
        this.ind_limpio_bano = ind_limpio_bano;
    }

    public String getInd_lav_diente() {
        return ind_lav_diente;
    }

    public void setInd_lav_diente(String ind_lav_diente) {
        this.ind_lav_diente = ind_lav_diente;
    }

    public String getInd_peinarse() {
        return ind_peinarse;
    }

    public void setInd_peinarse(String ind_peinarse) {
        this.ind_peinarse = ind_peinarse;
    }

    public String getInd_vestirse() {
        return ind_vestirse;
    }

    public void setInd_vestirse(String ind_vestirse) {
        this.ind_vestirse = ind_vestirse;
    }

    public String getInd_comer() {
        return ind_comer;
    }

    public void setInd_comer(String ind_comer) {
        this.ind_comer = ind_comer;
    }

    public String getInd_tomar() {
        return ind_tomar;
    }

    public void setInd_tomar(String ind_tomar) {
        this.ind_tomar = ind_tomar;
    }

    public String getInd_hablar() {
        return ind_hablar;
    }

    public void setInd_hablar(String ind_hablar) {
        this.ind_hablar = ind_hablar;
    }

    public String getInd_ver() {
        return ind_ver;
    }

    public void setInd_ver(String ind_ver) {
        this.ind_ver = ind_ver;
    }

    public String getInd_ayudar_casa() {
        return ind_ayudar_casa;
    }

    public void setInd_ayudar_casa(String ind_ayudar_casa) {
        this.ind_ayudar_casa = ind_ayudar_casa;
    }

    public String getInd_escuela() {
        return ind_escuela;
    }

    public void setInd_escuela(String ind_escuela) {
        this.ind_escuela = ind_escuela;
    }

    public String getInd_comunicacion() {
        return ind_comunicacion;
    }

    public void setInd_comunicacion(String ind_comunicacion) {
        this.ind_comunicacion = ind_comunicacion;
    }

    public String getInd_amigos() {
        return ind_amigos;
    }

    public void setInd_amigos(String ind_amigos) {
        this.ind_amigos = ind_amigos;
    }

    public String getInd_pasa_tiempos() {
        return ind_pasa_tiempos;
    }

    public void setInd_pasa_tiempos(String ind_pasa_tiempos) {
        this.ind_pasa_tiempos = ind_pasa_tiempos;
    }

    public AnamnesisClinico getIdanamnesis() {
        return idanamnesis;
    }

    public void setIdanamnesis(AnamnesisClinico idanamnesis) {
        this.idanamnesis = idanamnesis;
    }

    public String getInd_oir() {
        return ind_oir;
    }

    public void setInd_oir(String ind_oir) {
        this.ind_oir = ind_oir;
    }
}

