package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "composicionfamiliar")
@Access(AccessType.FIELD)
public class ComposicionFamiliar implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_compfamiliar")
    private Long id_compfamiliar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 50)
    @Column(name = "apellido")
    private String apellido;

    @Column(name = "edad")
    private Integer edad;
    @Size(max = 20)
    @Column(name = "parentesco")
    private String parentesco;
    @Size(max = 100)
    @Column(name = "ocupacion")
    private String ocupacion;
    @Size(max = 50)
    @Column(name = "gradoinstr")
    private String gradoinstr;

    @Column(name = "ingresomensual")
    private Double ingresomensual;
    @Size(max = 100)
    @Column(name = "observacion")
    private String observacion;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public Long getId_compfamiliar() {
        return id_compfamiliar;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public void setId_compfamiliar(Long id_compfamiliar) {
        this.id_compfamiliar = id_compfamiliar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getGradoinstr() {
        return gradoinstr;
    }

    public void setGradoinstr(String gradoinstr) {
        this.gradoinstr = gradoinstr;
    }

    public Double getIngresomensual() {
        return ingresomensual;
    }

    public void setIngresomensual(Double ingresomensual) {
        this.ingresomensual = ingresomensual;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }



    public ComposicionFamiliar(String nombre, String apellido, Integer edad, String parentesco, String ocupacion, String gradoinstr, Double ingresomensual, String observacion, Expediente idexpediente) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.parentesco = parentesco;
        this.ocupacion = ocupacion;
        this.gradoinstr = gradoinstr;
        this.ingresomensual = ingresomensual;
        this.observacion = observacion;
        this.idexpediente = idexpediente;
    }

    public ComposicionFamiliar() {
    }

    public ComposicionFamiliar(Long id_compfamiliar) {
        this.id_compfamiliar = id_compfamiliar;
    }
}
