package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Departamento;
import com.wasi.wasisoft.model.Distrito;
import com.wasi.wasisoft.model.Provincia;

import java.util.List;

public interface UbigeoService {

    List<Departamento> findAll();

    List<Provincia> findByIddepa(Integer iddepa);

    List<Distrito> findByIdprov(Integer id_prov);


}
