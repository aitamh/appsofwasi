package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ResultadoPruebas;

import java.util.List;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
public interface ResultadoPruebasService {

    ResultadoPruebas saveResultados(ResultadoPruebas resultadoPruebas);

    void deleleteResultados(Long idresultado);

    List<ResultadoPruebas> getResultadoPruebasPaciente(Expediente idexpediete, Area idarea);
}
