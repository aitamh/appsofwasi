package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionecon;

import java.util.List;

public interface SituacioneconService {

    /**
     * Registra y actualiza la SituacionEconomica del paciente
     * @param situacionecon
     * @return
     */
    Situacionecon save(Situacionecon situacionecon);
    List<Situacionecon> findAllSituacioecon();

    /**
     * Retorna la situacion economica del paciente.
     * @param idexpediente
     * @return
     */
    List<Situacionecon> obtenerSituacionEconomica(Expediente idexpediente);
}
