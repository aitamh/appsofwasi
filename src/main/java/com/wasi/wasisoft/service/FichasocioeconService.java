package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.model.VisitaDomiciliaria;

import java.util.List;

public interface FichasocioeconService {

    Fichasocioecon save(Fichasocioecon fichasocioecon);
    List<Fichasocioecon> findAll();
    void deleteFichasocioecon(Long id_fichasocioecon);

    /**
     * Devuelve la ficha socio economica de un paciente que se busca mediante su id de expediente.
     * @param idexpediente
     * @return
     */
    List<Fichasocioecon> obtenerFichaSocioEconomica(Expediente idexpediente);

//    VisitaDomiciliaria save(VisitaDomiciliaria visitaDomiciliaria);
//    List<VisitaDomiciliaria> findAllVisitaDomiciliaria();


}
