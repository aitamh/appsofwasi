package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.PreinscripcionDao;
import com.wasi.wasisoft.model.Preinscripcion;
import com.wasi.wasisoft.service.PreinscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PreinscripcionServiceImpl implements PreinscripcionService {

    @Autowired
    protected PreinscripcionDao preinscripcionDao;

    @Override
    public Preinscripcion save(Preinscripcion preinscripcion) {
        return this.preinscripcionDao.save(preinscripcion) ;
    }

    @Override
    public List<Preinscripcion> findAllDadosDeAlta() {
        return this.preinscripcionDao.findAllDadosDeAlta();
    }

    @Override
    public List<Preinscripcion> findAllDadosDeBaja() {
        return this.preinscripcionDao.findAllDadosDeBaja();
    }

    @Override
    public void deletePreinscripcion(Long id) {
        this.preinscripcionDao.deleteById(id);
    }
}
