package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ApoderadoDao;
import com.wasi.wasisoft.model.Apoderado;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.service.ApoderadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApoderadoServiceImpl implements ApoderadoService {

    @Autowired
    protected ApoderadoDao apoderadoDao;

    @Override
    public Apoderado save(Apoderado apoderado) {
        return this.apoderadoDao.save(apoderado);
    }

    @Override
    public List<Apoderado> findAll() {
        return this.apoderadoDao.findAll();
    }

    @Override
    public void deleteApoderado(Long id_apoderado) {
        this.apoderadoDao.deleteById(id_apoderado);
    }

    @Override
    public List<Apoderado> findByIdexpediente(Expediente idexpediente) {
        return this.apoderadoDao.findByIdexpediente(idexpediente);
    }
}
