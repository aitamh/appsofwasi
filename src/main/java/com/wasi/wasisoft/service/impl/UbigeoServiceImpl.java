package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.DepartamentoDao;
import com.wasi.wasisoft.dao.DistritoDao;
import com.wasi.wasisoft.dao.ProvinciaDao;
import com.wasi.wasisoft.model.Departamento;
import com.wasi.wasisoft.model.Distrito;
import com.wasi.wasisoft.model.Provincia;
import com.wasi.wasisoft.service.UbigeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UbigeoServiceImpl implements UbigeoService {

    @Autowired
    protected DepartamentoDao departamentoDao;

    @Autowired
    protected ProvinciaDao provinciaDao;

    @Autowired
    protected DistritoDao distritoDao;

    @Override
    public List<Departamento> findAll() {
        return this.departamentoDao.findAll();
    }

    @Override
    public List<Provincia> findByIddepa(Integer iddepa) {
        return this.provinciaDao.findByIddepa(iddepa);
    }

    @Override
    public List<Distrito> findByIdprov(Integer id_prov) {
        return this.distritoDao.findByIdprov(id_prov);
    }
}
