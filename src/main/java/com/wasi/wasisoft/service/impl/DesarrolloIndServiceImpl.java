package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.DesarrolloIndDao;
import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.DesarrolloInd;
import com.wasi.wasisoft.service.DesarrolloIndService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesarrolloIndServiceImpl  implements DesarrolloIndService {

    @Autowired
    protected DesarrolloIndDao desarrolloIndDao;

    @Override
    public DesarrolloInd save(DesarrolloInd desarrolloInd) {
        return desarrolloIndDao.save(desarrolloInd);
    }

    @Override
    public List<DesarrolloInd> findDesarrolloIndByIdanamnesisclinico(AnamnesisClinico idanamnesis) {
        return desarrolloIndDao.findByIdanamnesis(idanamnesis);
    }

    @Override
    public void deleteDesarrolloInd(Long iddesarrollo) {
        this.desarrolloIndDao.deleteById(iddesarrollo);
    }
}
