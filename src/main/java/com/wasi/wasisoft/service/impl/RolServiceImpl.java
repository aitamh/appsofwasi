package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.RoleRepository;
import com.wasi.wasisoft.model.Role;
import com.wasi.wasisoft.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolServiceImpl implements RolService {

    @Autowired
    protected RoleRepository roleRepository;

    @Override
    public Role save(Role rol) {
        return this.roleRepository.save(rol);
    }

    @Override
    public List<Role> findAll() {
        return this.roleRepository.findAll();
    }

    @Override
    public void deleteRol(Long id) {
        this.roleRepository.deleteById(id);
    }
}
