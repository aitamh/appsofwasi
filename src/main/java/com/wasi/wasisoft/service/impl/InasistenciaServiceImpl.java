package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.InasistenciaDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Inasistencia;
import com.wasi.wasisoft.service.InasistenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public  class InasistenciaServiceImpl implements InasistenciaService{

    @Autowired
    protected InasistenciaDao inasistenciaDao;


    @Override
    public Inasistencia saveInasistencia(Inasistencia inasistencia) {
        return this.inasistenciaDao.save(inasistencia);
    }

    @Override
    public List<Inasistencia> findAllInasistencia(FichaTerapiaFisica idfichaterapiafisica) {
        return this.inasistenciaDao.findAllByIdfichaterapiafisica(idfichaterapiafisica);
    }

    @Override
    public void deleteInasistenecia(Long idinasistencia) {
        this.inasistenciaDao.deleteById(idinasistencia);
    }
}
