package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.InspBocaArribaDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.InspBocaArriba;
import com.wasi.wasisoft.service.InspBocaArribaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InspBocaArribaServiceImpl implements InspBocaArribaService {

    @Autowired
    protected InspBocaArribaDao inspBocaArribaDao;

    @Override
    public InspBocaArriba save(InspBocaArriba inspBocaArriba) {
        return this.inspBocaArribaDao.save(inspBocaArriba);
    }

    @Override
    public List<InspBocaArriba> findInspBocaArribaByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapia) {
        return this.inspBocaArribaDao.findByIdfichaterapiafisica(idfichaterapia);
    }

    @Override
    public void deleteInspeccionBocaArriba(Long idinspeccion) {
        this.inspBocaArribaDao.deleteById(idinspeccion);
    }
}
