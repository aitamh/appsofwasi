package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.FichaTerapiaFisicaDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.FichaTerapiaFisicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FichaTerapiaFisicaServiceImpl implements FichaTerapiaFisicaService{

    @Autowired
    protected FichaTerapiaFisicaDao fichaTerapiaFisicaDao;

    @Override
    public FichaTerapiaFisica save(FichaTerapiaFisica fichaTerapiaFisica) {
        return this.fichaTerapiaFisicaDao.save(fichaTerapiaFisica);
    }

//    @Override
//    public List<FichaTerapiaFisica> findAllPacienteNoAtendido() {
//        return this.fichaTerapiaFisicaDao.findAllPacientoNoAtendido();
//    }

    @Override
    public void deleteFichaTerapiaFisica(Long idfichaterapia) {
        this.fichaTerapiaFisicaDao.deleteById(idfichaterapia);
    }

    @Override
    public List<FichaTerapiaFisica> ListarPacientesAtedidos() {
        return this.fichaTerapiaFisicaDao.findAllPacienteAtendido();
    }

    @Override
    public List<FichaTerapiaFisica> obtenerFichaTerapiaFisica(Expediente idexpediente) {
        return this.fichaTerapiaFisicaDao.findByIdexpediente(idexpediente);
    }
}
