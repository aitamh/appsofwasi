package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.*;
import com.wasi.wasisoft.model.*;
import com.wasi.wasisoft.service.FichaEducacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
@Service
public class FichaEducacionServiceImpl implements FichaEducacionService {

    @Autowired
    protected FichaEducacionDao fichaEducacionDao;

    @Autowired
    protected DesalenguajeDao desalenguajeDao;

    @Autowired
    protected DesarrolloSocialDao desarrolloSocialDao;

    @Autowired
    protected SaludActualDao saludActualDao;

    @Autowired
    protected EscolaridadDao escolaridadDao;

    @Autowired
    protected EvalSemestralDao evalSemestralDao;

    @Override
    public FichaEducacion savefichaeducacion(FichaEducacion fichaEducacion) {
        return this.fichaEducacionDao.save(fichaEducacion);
    }

    @Override
    public void deleteFichaEducacion(Long idfichaeduc) {
        this.fichaEducacionDao.deleteById(idfichaeduc);
    }

    @Override
    public List<FichaEducacion> getFichaEducacionPaciente(Expediente idexpediente) {
        return this.fichaEducacionDao.findAllByIdexpediente(idexpediente);
    }

    @Override
    public Desalenguaje saveDesarrolloLenguaje(Desalenguaje desalenguaje) {
        return this.desalenguajeDao.save(desalenguaje);
    }

    @Override
    public void deletDesarrollolenguaje(Long iddesarrollo) {
        this.desalenguajeDao.deleteById(iddesarrollo);
    }

    @Override
    public List<Desalenguaje> getDesarrolloLenguaje(FichaEducacion fichaEducacion) {
        return this.desalenguajeDao.findByIdfichaeduc(fichaEducacion);
    }

    @Override
    public DesarrolloSocial saveDesarrolloSocial(DesarrolloSocial desarrolloSocial) {
        return this.desarrolloSocialDao.save(desarrolloSocial);
    }

    @Override
    public void deleteDesarrolloSocial(Long iddesarrollo) {
        this.desarrolloSocialDao.deleteById(iddesarrollo);
    }

    @Override
    public List<DesarrolloSocial> getDesarrolloSocialPaciente(FichaEducacion idfichaeducacion) {
        return this.desarrolloSocialDao.findByIdfichaeduc(idfichaeducacion);
    }

    @Override
    public SaludActual saveSaludActual(SaludActual saludActual) {
        return this.saludActualDao.save(saludActual);
    }

    @Override
    public void deleteSaludActual(Long idsaludactual) {
        this.saludActualDao.deleteById(idsaludactual);
    }

    @Override
    public List<SaludActual> getSaludActualPaciente(FichaEducacion idfichaeducacion) {
        return this.saludActualDao.findByIdfichaeduc(idfichaeducacion);
    }

    @Override
    public Escolaridad saveEscolaridad(Escolaridad escolaridad) {
        return this.escolaridadDao.save(escolaridad);
    }

    @Override
    public void deleteEscolaridad(Long idescolaridad) {
        this.escolaridadDao.deleteById(idescolaridad);
    }

    @Override
    public List<Escolaridad> getEscolaridadPaciente(FichaEducacion idfichaeducacion) {
        return this.escolaridadDao.findByIdfichaeduc(idfichaeducacion);
    }

    @Override
    public EvalSemestral saveEvalSemestral(EvalSemestral evalSemestral) {
        return this.evalSemestralDao.save( evalSemestral);
    }

    @Override
    public void deleteEvalsemestral(Long idevalSemestral) {
        this.evalSemestralDao.deleteById(idevalSemestral);
    }

    @Override
    public List<EvalSemestral> getEvaluacionSemestral(FichaEducacion idfichaeducacion, Integer numevaluacion) {
        return this.evalSemestralDao.findAllByIdfichaeducacionAndNumevaluacion(idfichaeducacion,numevaluacion);
    }
}
