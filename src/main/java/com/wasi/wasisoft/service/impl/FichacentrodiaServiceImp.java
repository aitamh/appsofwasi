package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.CuidadoAlimDao;
import com.wasi.wasisoft.dao.FichacentrodiaDao;
import com.wasi.wasisoft.model.CuidadoAlim;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichacentrodia;
import com.wasi.wasisoft.service.FichacentrodiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 27/04/2018.
 */
@Service
public class FichacentrodiaServiceImp implements FichacentrodiaService {

    @Autowired
    protected FichacentrodiaDao fichacentrodiaDao;

    @Autowired
    protected CuidadoAlimDao cuidadoAlimDao;

    @Override
    public Fichacentrodia saveFichcebtrodia(Fichacentrodia fichacentrodia) {
        return this.fichacentrodiaDao.save(fichacentrodia);
    }

    @Override
    public void deleteFichacentrodia(Long idfichacentrodia) {
        this.fichacentrodiaDao.deleteById(idfichacentrodia);
    }

    @Override
    public List<Fichacentrodia> getFichacentrodiaPaciente(CuidadoAlim cuidadoAlim) {
        return this.fichacentrodiaDao.findAllByIdcuidalim(cuidadoAlim);
    }

}
