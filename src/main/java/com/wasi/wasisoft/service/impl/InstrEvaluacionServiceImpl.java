package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.InstrEvaluacionDao;
import com.wasi.wasisoft.model.InstrEvaluacion;
import com.wasi.wasisoft.service.InstrEvaluacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernadez on 03/05/2018.
 */
@Service
public class InstrEvaluacionServiceImpl implements InstrEvaluacionService {

    @Autowired
    protected InstrEvaluacionDao instrEvaluacionDao;

    @Override
    public InstrEvaluacion saveInstrEvaluacion(InstrEvaluacion instrEvaluacion) {
        return this.instrEvaluacionDao.save(instrEvaluacion);
    }

    @Override
    public void deleteInstrEvaluacion(Long idinstrumento) {
        this.instrEvaluacionDao.deleteById(idinstrumento);
    }

    @Override
    public List<InstrEvaluacion> getInstrumentosEvaluacion() {
        return this.instrEvaluacionDao.findAllOrOrderByNombreprueba();
    }
}
