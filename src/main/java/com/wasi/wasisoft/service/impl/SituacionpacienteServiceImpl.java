package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.SituacioneconDao;
import com.wasi.wasisoft.dao.SituacionpacienteDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionpaciente;
import com.wasi.wasisoft.service.SituacionpacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SituacionpacienteServiceImpl implements SituacionpacienteService {

    @Autowired
    protected SituacionpacienteDao situacionpacienteDao;

    @Override
    public Situacionpaciente save(Situacionpaciente situacionpaciente) {
        return this.situacionpacienteDao.save(situacionpaciente);
    }

    @Override
    public List<Situacionpaciente> findByIdexpediente(Expediente idexpediente) {
        return this.situacionpacienteDao.findByIdexpediente(idexpediente);
    }

    @Override
    public List<Situacionpaciente> findAllExpedienteByPacienteactivo() {
        return this.situacionpacienteDao.findAllExpedienteByPacienteactivo();
    }

    @Override
    public List<Situacionpaciente> getPacienteActivoAndAceptado() {
        return this.situacionpacienteDao.findAllPacienteActivoAndAceptado();
    }
}
