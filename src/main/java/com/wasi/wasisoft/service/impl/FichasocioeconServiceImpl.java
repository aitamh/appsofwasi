package com.wasi.wasisoft.service.impl;


import com.wasi.wasisoft.dao.FichasocioeconDao;
import com.wasi.wasisoft.dao.SituacioneconDao;
import com.wasi.wasisoft.dao.VisitaDomiciliariaDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import com.wasi.wasisoft.service.FichasocioeconService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FichasocioeconServiceImpl implements FichasocioeconService {

    @Autowired
    protected FichasocioeconDao fichasocioeconDao;

    @Override
    public Fichasocioecon save(Fichasocioecon fichasocioecon) {
        return this.fichasocioeconDao.save(fichasocioecon);
    }

    @Override
    public List<Fichasocioecon> findAll() {
        return this.fichasocioeconDao.findAll();
    }

    @Override
    public void deleteFichasocioecon(Long id_fichasocioecon) {
        this.fichasocioeconDao.deleteById(id_fichasocioecon);
    }

    @Override
    public List<Fichasocioecon> obtenerFichaSocioEconomica(Expediente idexpediente) {
        return this.fichasocioeconDao.findByIdexpediente(idexpediente);
    }


//    @Override
//    public VisitaDomiciliaria save(VisitaDomiciliaria visitaDomiciliaria) {
//        return this.visitaDomiciliariaDao.save(visitaDomiciliaria);
//    }
//
//    @Override
//    public List<VisitaDomiciliaria> findAllVisitaDomiciliaria() {
//        return this.visitaDomiciliariaDao.findAll();
//    }
}
