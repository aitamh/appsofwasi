package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.AnamnesisFamiliarDao;
import com.wasi.wasisoft.model.AnamnesisFamiliar;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.AnamnesisFamiliarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnamnesisFamiliarServiceImpl implements AnamnesisFamiliarService {

    @Autowired
    protected AnamnesisFamiliarDao anamnesisFamiliarDao;

    @Override
    public AnamnesisFamiliar save(AnamnesisFamiliar anamnesisFamiliar) {
        return this.anamnesisFamiliarDao.save(anamnesisFamiliar);
    }

    @Override
    public List<AnamnesisFamiliar> getAnamnesisFamiliar(FichaTerapiaFisica idfichaTerapiaFisica) {
        return this.anamnesisFamiliarDao.findByIdfichaterapiafisica(idfichaTerapiaFisica);
    }

    @Override
    public void delete(Long idanamnesisfamiliar) {
        this.anamnesisFamiliarDao.deleteById(idanamnesisfamiliar);
    }
}
