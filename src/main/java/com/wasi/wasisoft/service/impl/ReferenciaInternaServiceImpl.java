package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ReferenciaInternaDao;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaInterna;
import com.wasi.wasisoft.service.ReferenciaInternaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@Service
public class ReferenciaInternaServiceImpl implements ReferenciaInternaService {

    @Autowired
    protected ReferenciaInternaDao referenciaInternaDao;

    @Override
    public ReferenciaInterna saveReferenciaInterna(ReferenciaInterna referenciaInterna) {
        return this.referenciaInternaDao.save(referenciaInterna);
    }

    @Override
    public void deleteReferenciaInterna(Long idreferencia) {
        this.referenciaInternaDao.deleteById(idreferencia);
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInterna() {
        return this.referenciaInternaDao.findAll();
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInternaPorAreas(Area idarea) {
        return this.referenciaInternaDao.findByIdarea(idarea);
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInternaPorPaciente(Expediente idexpediente) {
        return this.referenciaInternaDao.findAllByIdexpediente(idexpediente);
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInternaValidar(Area idarea, Expediente idexpediente) {
        return this.referenciaInternaDao.findByIdareaAndIdexpediente(idarea, idexpediente);
    }
}
