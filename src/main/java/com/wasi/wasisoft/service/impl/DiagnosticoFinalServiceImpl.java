package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.DiagnosticoFinalDao;
import com.wasi.wasisoft.dao.PlanTrabajoDao;
import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.PlanTrabajo;
import com.wasi.wasisoft.service.DiagnosticoFinalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
@Service
public class DiagnosticoFinalServiceImpl implements DiagnosticoFinalService {

    @Autowired
    protected DiagnosticoFinalDao diagnosticoFinalDao;

    @Autowired
    protected PlanTrabajoDao planTrabajoDao;

    @Override
    public DiagnosticoFinal saveDiagnosticoFinal(DiagnosticoFinal diagnosticoFinal) {
        return this.diagnosticoFinalDao.save(diagnosticoFinal);
    }

    @Override
    public void deleteDiagnosticoFinal(Long iddiagnostico) {
        this.diagnosticoFinalDao.deleteById(iddiagnostico);
    }

    @Override
    public List<DiagnosticoFinal> getDiagnosticoFinal(FichaTerapiaFisica idfichaterapia) {
        return this.diagnosticoFinalDao.findByIdfichaterapiafisica(idfichaterapia);
    }

    @Override
    public PlanTrabajo savePlanTrabajo(PlanTrabajo planTrabajo) {
        return planTrabajoDao.save(planTrabajo);
    }

    @Override
    public void deletePlanTrabajo(Long idplantrabajo) {
        this.planTrabajoDao.deleteById(idplantrabajo);
    }

    @Override
    public List<PlanTrabajo> getPLanDeTrabajoDelPaciente(DiagnosticoFinal iddiagnostico) {
        return planTrabajoDao.findAllByIddiagnosticofinal(iddiagnostico);
    }
}
