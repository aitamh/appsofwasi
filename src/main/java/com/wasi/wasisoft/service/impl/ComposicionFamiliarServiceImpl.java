package com.wasi.wasisoft.service.impl;


import com.wasi.wasisoft.dao.ComposicionFamiliarDao;
import com.wasi.wasisoft.model.ComposicionFamiliar;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.service.ComposicionFamiliarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComposicionFamiliarServiceImpl implements ComposicionFamiliarService {

    @Autowired
    protected ComposicionFamiliarDao composicionFamiliarDao;
    @Override
    public List<ComposicionFamiliar> findAll() {
        return this.composicionFamiliarDao.findAll();
    }

    @Override
    public ComposicionFamiliar save(ComposicionFamiliar composicionFamiliar) {
        return this.composicionFamiliarDao.save(composicionFamiliar);
    }

    @Override
    public void deleteComposicionFamiliar(Long idcompfamiliar) {
        this.composicionFamiliarDao.deleteById(idcompfamiliar);
    }

    @Override
    public List<ComposicionFamiliar> findByIdexpediente(Expediente idexpediente) {
        return this.composicionFamiliarDao.findByIdexpediente(idexpediente);
    }
}
