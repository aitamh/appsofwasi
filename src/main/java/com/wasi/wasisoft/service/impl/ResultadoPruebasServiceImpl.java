package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ResultadoPruebasDao;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ResultadoPruebas;
import com.wasi.wasisoft.service.ResultadoPruebasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
@Service
public class ResultadoPruebasServiceImpl implements ResultadoPruebasService {

    @Autowired
    protected ResultadoPruebasDao resultadoPruebasDao;

    @Override
    public ResultadoPruebas saveResultados(ResultadoPruebas resultadoPruebas) {
        return this.resultadoPruebasDao.save(resultadoPruebas);
    }

    @Override
    public void deleleteResultados(Long idresultado) {
        this.resultadoPruebasDao.deleteById(idresultado);
    }

    @Override
    public List<ResultadoPruebas> getResultadoPruebasPaciente(Expediente idexpediete, Area idarea) {
        return this.resultadoPruebasDao.findAllByIdexpedienteAndAndIdarea(idexpediete, idarea);
    }
}
