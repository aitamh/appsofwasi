package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.BiometriaDao;
import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.BiometriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BiometriaServiceImpl implements BiometriaService {

    @Autowired
    protected BiometriaDao biometriaDao;

    @Override
    public Biometria saveBiometria(Biometria biometria) {
        return this.biometriaDao.save(biometria);
    }

    @Override
    public List<Biometria> getBiometriaPaciente(FichaTerapiaFisica idfichaterapiafisica) {
        return this.biometriaDao.findByIdfichaterapiafisica(idfichaterapiafisica);
    }

    @Override
    public void deleteBiometria(Long idbiometria) {
        this.biometriaDao.deleteById(idbiometria);
    }
}
