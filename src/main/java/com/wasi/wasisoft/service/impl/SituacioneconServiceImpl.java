package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.SituacioneconDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.service.SituacioneconService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SituacioneconServiceImpl implements SituacioneconService{


    @Autowired
    protected SituacioneconDao situacioneconDao;

    @Override
    public Situacionecon save(Situacionecon situacionecon) {
        return this.situacioneconDao.save(situacionecon);
    }

    @Override
    public List<Situacionecon> findAllSituacioecon() {
        return this.situacioneconDao.findAll();
    }

    @Override
    public List<Situacionecon> obtenerSituacionEconomica(Expediente idexpediente) {
        return this.situacioneconDao.findByIdexpediente(idexpediente);
    }
}
