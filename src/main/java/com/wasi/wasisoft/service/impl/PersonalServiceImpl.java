package com.wasi.wasisoft.service.impl;


import com.wasi.wasisoft.dao.PersonalDao;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.service.PersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalServiceImpl implements PersonalService {

    @Autowired
    protected PersonalDao userDao;

    @Override
    public Personal save(Personal personal) {
        return this.userDao.save(personal);
    }

    @Override
    public List<Personal> findAll() {
        return this.userDao.findAll();
    }

    @Override
    public void deletePersonal(Long id) {
        this.userDao.deleteById(id);
    }

    @Override
    public List<Personal> findPersonalByAreaRehabilitacion() {
        return this.userDao.findPersonalByAreaRehabilitacion();
    }

    @Override
    public List<Personal> findAllByIdarea(Area idarea) {
        return  this.userDao.findAllByIdarea(idarea);
    }
}
