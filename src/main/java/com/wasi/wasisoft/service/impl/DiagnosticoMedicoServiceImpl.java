package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.DiagnosticoMedicoDao;
import com.wasi.wasisoft.model.DiagnosticoMedico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.DiagnosticoMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiagnosticoMedicoServiceImpl implements DiagnosticoMedicoService {

    @Autowired
    protected DiagnosticoMedicoDao diagnosticoMedicoDao;

    @Override
    public DiagnosticoMedico save(DiagnosticoMedico diagnosticoMedico) {
        return this.diagnosticoMedicoDao.save(diagnosticoMedico);
    }

    @Override
    public List<DiagnosticoMedico> getDiagnosticoMedico(FichaTerapiaFisica idfichaterapiafisica) {
        return this.diagnosticoMedicoDao.findByIdfichaterapiafisica(idfichaterapiafisica);
    }

    @Override
    public void deleteDiagnosticoMedico(Long idDiagnosticoMedico) {
        this.diagnosticoMedicoDao.deleteById(idDiagnosticoMedico);
    }
}
