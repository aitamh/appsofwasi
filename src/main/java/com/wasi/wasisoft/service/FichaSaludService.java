package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.*;

import java.util.List;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
public interface FichaSaludService {

    FichaSalud saveFichaSalud(FichaSalud fichaSalud);

    void deleteFichaSalud(Long idfichasalud);

    /**
     * Obtiene la ficha salud del paciente indicado
     * @param idcuidadoalim
     * @return
     */
    List<FichaSalud> getFichaSaludPaciente(CuidadoAlim idcuidadoalim);

    //==========METODOS DE VISITA MEDICA==============

    VisitaMedica saveVisitaMedica(VisitaMedica visitaMedica);

    void deleteVisitaMedica(Long Idvisita);

    List<VisitaMedica> getVisitaMedicaPaciente(FichaSalud idfichasalud);

    //========METODOS DE VISITA DOMICILIARIA INTEGRAL===============
    Visdomintegral saveVisdomintegral(Visdomintegral visdomintegral);

    void deleteVisdomintegral(Long idvisdomintegral);

    /**
     * obtiene la visita domiciliaria integral del paciente
     * @param idfichasalud
     * @return
     */
    List<Visdomintegral> getVisitaDomiciliariaIntegral(FichaSalud idfichasalud);

    //=========METODOS DE CuidAlim (CUIDADO Y ALIMENTACION) =================
    CuidadoAlim saveCuidadoAlim(CuidadoAlim cuidadoAlim);

    void deleteCuidadoAlim(Long idcuidadoalim);

    /**
     * Obtiene datos de alimentacion y cuidado del paciente
     * @param idexpediente
     * @return
     */
    List<CuidadoAlim> getCuidadoAlimentacionPaciente(Expediente idexpediente);
}
