package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.*;

import java.util.List;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
public interface FichaEducacionService {

    FichaEducacion savefichaeducacion(FichaEducacion fichaEducacion);

    void deleteFichaEducacion(Long idfichaeduc);

    List<FichaEducacion> getFichaEducacionPaciente(Expediente idexpediente);

    //========METODOS DEL DESARROLLO DEL LENGUAJE (Desalenguaje)====================================================

    Desalenguaje saveDesarrolloLenguaje(Desalenguaje desalenguaje);

    void deletDesarrollolenguaje(Long iddesarrollo);

    List<Desalenguaje> getDesarrolloLenguaje(FichaEducacion fichaEducacion);

    //=======METODOS DE DESARROLLO SOCIAL ===========================================================================

    DesarrolloSocial saveDesarrolloSocial(DesarrolloSocial desarrolloSocial);

    void deleteDesarrolloSocial(Long iddesarrollo);

    List<DesarrolloSocial> getDesarrolloSocialPaciente(FichaEducacion idfichaeducacion);

    //=============METODOS DE LA SALUD ACTUAL (SaludAcutual)==========================================================

    SaludActual saveSaludActual(SaludActual saludActual);

    void deleteSaludActual(Long idsaludactual);

    List<SaludActual> getSaludActualPaciente(FichaEducacion idfichaeducacion);

    //===========METODOS DE ESCOLARIDAD (Escolaridad) DEL PACIENTE====================================================

    Escolaridad saveEscolaridad(Escolaridad escolaridad);

    void deleteEscolaridad(Long idescolaridad);

    List<Escolaridad> getEscolaridadPaciente(FichaEducacion idfichaeducacion);

    //===========METODOS DE EVALUACION SEMESTRAL DEL PACIENTE====================================================

    EvalSemestral saveEvalSemestral(EvalSemestral evalSemestral);

    void deleteEvalsemestral(Long idevalSemestral);

    /**
     * Obtiene la evaluacion semestral del paciente
     * @param idfichaeducacion
     * @param numevaluacion
     * @return
     */
    List<EvalSemestral> getEvaluacionSemestral(FichaEducacion idfichaeducacion, Integer numevaluacion);
}
