package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Movilidad;
import com.wasi.wasisoft.model.MovilidadArticulaciones;

import java.util.List;

public interface MovilidadArticulacionesService {

    /**
     * Registra y actualiza
     * @param movilidadArticulaciones
     * @return
     */
    MovilidadArticulaciones save(MovilidadArticulaciones movilidadArticulaciones);

    /**
     * Retorna la Movilidad de las articulaciones de un paciente
     * @param idfichaTerapiaFisica
     * @return
     */
    List<MovilidadArticulaciones> getMovilidadArticulacionesPaciente(FichaTerapiaFisica idfichaTerapiaFisica);

    /**
     * Elimina un registro
     * @param idmovilidad
     */
    void deleteMovilidadArticulaciones(Long idmovilidad);

    //METODOS DE  MOVILIDAD DEL PACIENTE

    /**
     * Registra y actualiza en la tabla movilidad
     * @param movilidad
     * @return
     */
    Movilidad saveMovilidadPaciente(Movilidad movilidad);

    /**
     * Elimina un registro de movilidad
     * @param idmovilidad
     */
    void deleteMovilidadPaciente(Long idmovilidad);

    /**
     * Retorna todos los seguimientos de movilidad que tiene el paciente
     * @param movilidadArticulaciones
     * @return
     */
    List<Movilidad> getMovilidadSeguimiento(MovilidadArticulaciones movilidadArticulaciones);

    /**
     * Retorna la movilidad de seguimiennto uno del paciente.
     * @return
     */
//    List<Movilidad> getMovilidadSeguimientoUno();
//
//    /**
//     * Retorna datos del segundo seguimiento de movilidad
//     * @return
//     */
//    List<Movilidad> getMovilidadSeguimientoDos();
//
//    /**
//     * Retorna datos del tercer seguimiento de movilidad.
//     * @return
//     */
//    List<Movilidad> getMovilidadSeguimientoTres();

}
