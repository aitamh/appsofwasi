package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Inasistencia;
import org.apache.catalina.LifecycleState;

import java.util.List;

public interface InasistenciaService {

    /**
     * Registra y actualiza
     * @param inasistencia
     * @return
     */
    Inasistencia saveInasistencia(Inasistencia inasistencia);

    /**
     * Lista todas las inasistencias que tiene un paciente.
     * @param idfichaterapiafisica
     * @return
     */
    List<Inasistencia> findAllInasistencia(FichaTerapiaFisica idfichaterapiafisica);

    /**
     * Si la cantidad de inasistencias es igual a 3, se elimina la lista inasistencias para ese paciente
     * @param idinasistencia
     */
    void deleteInasistenecia(Long idinasistencia);
}
