package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.InstrEvaluacion;

import java.util.List;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
public interface InstrEvaluacionService {

    InstrEvaluacion saveInstrEvaluacion(InstrEvaluacion instrEvaluacion);

    void deleteInstrEvaluacion(Long idinstrumento);

    List<InstrEvaluacion> getInstrumentosEvaluacion();
}
