package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.ResultadoEscala;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface ResultadoEscalaService {

    ResultadoEscala saveResultadoEscala(ResultadoEscala resultadoEscala);

    void deleteResultadoEscala(Long idResultado);

    List<ResultadoEscala> getResultadoEscalaPaciente(FichaTerapiaFisica idfichaterapiafisica);
}
