package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Preinscripcion;

import java.util.List;

public interface PreinscripcionService {


        /**
         * Guarda un PreinscripcionDao
         *
         * @param preinscripcion
         * @return el preinscripcion guardado
         */
        Preinscripcion save(Preinscripcion preinscripcion);

        /**
         * Recupera la lista de preinscripcion
         *
         * @return lista de preinscripcion
         */
        List<Preinscripcion> findAllDadosDeAlta();

        List<Preinscripcion> findAllDadosDeBaja();
        /**
         * Elimina un preinscricion con el id recibido
         *
         * @param id
         */
        void deletePreinscripcion(Long id);
}
