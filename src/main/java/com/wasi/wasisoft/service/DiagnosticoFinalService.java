package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.PlanTrabajo;

import java.util.List;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
public interface DiagnosticoFinalService {

    DiagnosticoFinal saveDiagnosticoFinal(DiagnosticoFinal diagnosticoFinal);

    void deleteDiagnosticoFinal(Long iddiagnostico);

    List<DiagnosticoFinal> getDiagnosticoFinal(FichaTerapiaFisica idfichaterapia);

    // METODOS DEL PLAN DE TRABAJO
    //===============================================

    PlanTrabajo savePlanTrabajo(PlanTrabajo planTrabajo);

    void deletePlanTrabajo(Long idplantrabajo);

    List<PlanTrabajo> getPLanDeTrabajoDelPaciente(DiagnosticoFinal iddiagnosticofinal);
}
