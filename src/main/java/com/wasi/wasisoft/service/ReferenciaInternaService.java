package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaInterna;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
public interface ReferenciaInternaService {

    ReferenciaInterna saveReferenciaInterna(ReferenciaInterna referenciaInterna);

    void  deleteReferenciaInterna(Long idreferencia);

    /**
     * Lista todas las referencias internas. de todas las áreas
     * @return
     */
    List<ReferenciaInterna> getReferenciaInterna();

    /**
     * Lista las refencias por areas.
     * @param idarea
     * @return
     */
    List<ReferenciaInterna> getReferenciaInternaPorAreas(Area idarea);

    /**
     * Lista todas las referencias que tiene un paciente.
     * @param idexpediente
     * @return
     */
    List<ReferenciaInterna> getReferenciaInternaPorPaciente(Expediente idexpediente);

    List<ReferenciaInterna> getReferenciaInternaValidar(Area idarea, Expediente idexpediente);
}
