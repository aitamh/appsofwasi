package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.AnamnesisFamiliar;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

public interface AnamnesisFamiliarService {

    /**
     * Registra y actualiza
     * @param anamnesisFamiliar
     * @return
     */
    AnamnesisFamiliar save(AnamnesisFamiliar anamnesisFamiliar);

    /**
     * retorna el anamnesi familiar del paciente
     * @param fichaTerapiaFisica
     * @return
     */
    List<AnamnesisFamiliar> getAnamnesisFamiliar(FichaTerapiaFisica fichaTerapiaFisica);

    /**
     * Eliminar registro
     * @param idanamnesisfamiliar
     */
    void delete(Long idanamnesisfamiliar);
}
