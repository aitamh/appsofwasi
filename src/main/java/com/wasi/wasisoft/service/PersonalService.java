package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import java.util.List;

public interface PersonalService {

    /**
     * Guarda un usuario
     *
     * @param personal
     * @return el usuario guardado
     */
    Personal save(Personal personal);

    /**
     * Recupera la lista de usuarios
     *
     * @return lista de usuarios
     */
    List<Personal> findAll();

    /**
     * Elimina un usuario con el id recibido
     *
     * @param id
     */
    void deletePersonal(Long id);

    List<Personal> findPersonalByAreaRehabilitacion();

    List<Personal> findAllByIdarea(Area idarea);
}
