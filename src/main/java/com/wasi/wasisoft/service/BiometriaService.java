package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

public interface BiometriaService {

    /**
     * Registra y actualiza un registro
     * @param biometria
     * @return
     */
    Biometria saveBiometria(Biometria biometria);

    /**
     * Muestra la biometria de un paciente
     * @return
     */
    List<Biometria> getBiometriaPaciente(FichaTerapiaFisica idfichaterapiafisica);

    /**
     * Elimina un registro de biometria
     * @param idbiometria
     */
    void deleteBiometria(Long idbiometria);
}
