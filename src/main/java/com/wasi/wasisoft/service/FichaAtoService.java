package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaAto;
import com.wasi.wasisoft.model.FichaSalud;

import java.util.List;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
public interface FichaAtoService {

    FichaAto saveFichAto(FichaAto fichaAto);

    void deleteFichaAto(Long idfichaato);

    List<FichaAto> getFichaAtoPaciente(Expediente idexpediente);
}
