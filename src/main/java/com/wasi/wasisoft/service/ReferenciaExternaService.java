package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaExterna;

import java.util.List;

public interface ReferenciaExternaService {

    /**
     * Registra y actualiza
     * @param referenciaExterna
     * @return
     */
    ReferenciaExterna save(ReferenciaExterna referenciaExterna);

    /**
     * Retorna la referencia externa del paciente
     * @param idexpediente
     * @return
     */
    List<ReferenciaExterna> findReferenciaExternaByIdexpediente(Expediente idexpediente);

    /**
     * Elimina un registro
     * @param idreferenciaexterna
     */
    void deleteReferenciaExterna(Long idreferenciaexterna);
}
