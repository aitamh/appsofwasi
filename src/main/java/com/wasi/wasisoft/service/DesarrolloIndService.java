package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.DesarrolloInd;

import java.util.List;

public interface DesarrolloIndService {

    /**
     * Registra y actualiza
     * @param desarrolloInd
     * @return
     */
    DesarrolloInd save(DesarrolloInd desarrolloInd);

    /**
     * Retorna el desarrollo e independencia del paciente
     * @param idanamnesis
     * @return
     */
    List<DesarrolloInd> findDesarrolloIndByIdanamnesisclinico(AnamnesisClinico idanamnesis);

    /**
     * Elimina un registro
     * @param iddesarrollo
     */
    void deleteDesarrolloInd(Long iddesarrollo);
}
