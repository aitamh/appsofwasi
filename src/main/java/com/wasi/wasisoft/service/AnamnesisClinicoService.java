package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

public interface AnamnesisClinicoService {

    /**
     * Registra y actualiza
     * @param anamnesisClinico
     * @return
     */
    AnamnesisClinico save(AnamnesisClinico anamnesisClinico);

    /**
     * Muestra el anamnesis clinico de un paciente, mediante una busqueda.
     * @param idfichaterapiafisica
     * @return
     */
    List<AnamnesisClinico> findByAnamnesisClinicoByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);

    void delete(Long idanamnesisclinico);
}
