package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Espasticidad;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

/**
 * Created by Espasticidad on 16/04/2018.
 */
public interface EspasticidadService {

    Espasticidad saveEspasticidad(Espasticidad espasticidad);

    void deleteEspasticidad(Long idespasticidad);

    List<Espasticidad> getEspasticidadPaciente(FichaTerapiaFisica idfichaterapia);
}
