package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Fuerza;
import com.wasi.wasisoft.model.FuerzaSeguimiento;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface FuerzaService {

    Fuerza saveFuerza(Fuerza fuerza);

    void deleteFuerza(Long idfuerza);

    List<Fuerza> getFuerzaPaciente(FichaTerapiaFisica idfichaterapiafisica);

    // METODOS DE LOS SEGUIMIENTOS DE LA FUERZA
    //=========================================================================

    FuerzaSeguimiento saveFuerzaSeguimiento(FuerzaSeguimiento fuerzaSeguimiento);

    void deleteSeguimientoFuerza(Long idseguimientofuerza);

    List<FuerzaSeguimiento> getSeguimientoFuerzaPaciente(Fuerza idfuerza);

}
