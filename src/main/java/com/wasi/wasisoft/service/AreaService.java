package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Area;
import java.util.List;

public interface AreaService {

    /**
     * Guarda un Area
     *
     * @param area
     * @return el rol guardado
     */
    Area save(Area area);

    /**
     * Recupera la lista de roles
     *
     * @return lista de roles
     */
    List<Area> findAll();

    /**
     * Elimina un area con el id recibido
     *
     * @param id
     */
    void deleteArea(Long id);

    /**
     * Obtiene una lista de areas para mostrar en referencia interna.
     */
    List<Area> getAreasReferenciaInterna();
}
